package org.nrg.xnat.tools.metadataextractor.exceptions;

public class AggregatorRecordParseException extends Exception {
    public AggregatorRecordParseException(String msg) {
        super(msg);
    }
}
