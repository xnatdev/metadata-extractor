package org.nrg.xnat.tools.metadataextractor.utils;

import org.nrg.xnat.tools.metadataextractor.entities.reports.ExampleAnalysisAggregate;
import org.nrg.xnat.tools.metadataextractor.entities.reports.SummaryAnalysisAggregate;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class JsonUtil {
    public static String escape(String str) {
        return str.replace("\\", "\\\\").replace("\"", "\\\"");
    }

    public static void write(Map<String, ExampleAnalysisAggregate.ValueInfo> elements, Writer writer, int numFilesPerValue, int numTruncate) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            writer.write("\"");
            writer.write(escape(tag));
            writer.write("\":{");

            ExampleAnalysisAggregate.ValueInfo valueInfo = elements.get(tag);

            writer.write("\"keyword\":\"");
            writer.write(escape(valueInfo.getKeyword()));
            writer.write("\",");

            Map<String, List<String>> fileNames = valueInfo.getFileNames();
            List<String> fileNamesKeys = new ArrayList<>(fileNames.keySet());
            Collections.sort(fileNamesKeys);

            int valueCount = numTruncate <= 0 ? fileNamesKeys.size() : Math.min(numTruncate, fileNamesKeys.size());
            for (int i = 0; i < valueCount ; i++) {
                writer.write("\"");
                writer.write(escape(fileNamesKeys.get(i)));
                writer.write("\":[");

                List<String> names = fileNames.get(fileNamesKeys.get(i));
                Collections.sort(names);

                // When numFilesPerValue == 0, don't display file names
                if (numFilesPerValue != 0) {
                    int count = numFilesPerValue == 0 ? names.size() : Math.min(numFilesPerValue, names.size());
                    for (int j = 0; j < count ; j++) {
                        writer.write("\"");
                        writer.write(escape(names.get(j)));
                        writer.write("\"");

                        if (j != count - 1) {
                            writer.write(",");
                        }
                    }
                }
                writer.write("]");

                if (i != valueCount - 1) {
                    writer.write(",");
                }
            }

            writer.write("}");
        }
    }

    public static void write(Map<String, SummaryAnalysisAggregate.Counter> elements, Writer writer) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            writer.write("\"");
            writer.write(escape(tag));
            writer.write("\":{");

            SummaryAnalysisAggregate.Counter valueInfo = elements.get(tag);

            writer.write("\"keyword\":\"");
            writer.write(escape(valueInfo.getKeyword()));
            writer.write("\",");

            Map<String, Integer> counters = valueInfo.getCounters();
            writer.write("\"counters\":{");
            int valueCount = 0;
            for (String value : counters.keySet()) {
                if (valueCount != 0) {
                    writer.write(",");
                }
                Integer count = counters.get(value);
                writer.write("\"");
                writer.write(escape(value));
                writer.write("\":");
                writer.write(Integer.toString(count));

                valueCount++;
            }
            writer.write("}");

            writer.write("}");
        }
    }


}
