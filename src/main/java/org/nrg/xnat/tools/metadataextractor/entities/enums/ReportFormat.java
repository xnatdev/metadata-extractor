package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum ReportFormat {
    json,
    xml,
    text;

    public static String getExtension(ReportFormat reportFormat) {
        switch(reportFormat) {
            case json:
                return ".json";

            case xml:
                return ".xml";

            case text:
                return ".txt";

            default:
                return "";
        }
    }
}
