package org.nrg.xnat.tools.metadataextractor.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.nrg.xnat.tools.metadataextractor.entities.enums.SupportedScheme;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

@Slf4j
public class FtpUtil {
    private FtpUtil() {
    }

    public static File download(URL url) throws IOException {
        Path path = null;
        FTPClient ftp = null;

        String protocol = url.getProtocol();
        SupportedScheme scheme = SupportedScheme.valueOf(protocol);

        log.debug("Retrieving a dictionary file from {} via FTP", url.toString());

        if (scheme == SupportedScheme.ftps) {
            FTPSClient ftps = new FTPSClient();
            ftps.setTrustManager(null);
            ftp = ftps;
        } else if (scheme == SupportedScheme.ftp) {
            ftp = new FTPClient();
        }

        try {
            String userInfo = url.getUserInfo();
            String username, password;
            if (userInfo == null) {
                log.debug("user info is not provided, guest will be used");
                username = "guest";
                password = "guest";
            } else {
                String[] tokens = userInfo.split(":");
                username = tokens[0];
                password = tokens[1];
                log.debug("Will login with username: {}", username);
            }
            int port = url.getPort();
            if (port > 0) {
                ftp.connect(url.getHost(), url.getPort());
            } else {
                ftp.connect(url.getHost());
            }
            if (!ftp.login(username, password)) {
                throw new IOException("Login failed");
            }

            int replyCode = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                throw new IOException("FTP Server refused connection");
            }
            path = FileUtil.createTempFile();
            log.debug("Saving the dictionary file to {}", path);
            FileOutputStream out = new FileOutputStream(path.toFile());
            if (!ftp.retrieveFile(url.getFile(), out)) {
                throw new IOException("Failed to retrieve the file: " + url);
            }
            out.close();
        } catch (IOException e) {
            throw e;
        } finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (final IOException f) {
                    // do nothing
                }
            }
        }
        return path.toFile();
    }
}
