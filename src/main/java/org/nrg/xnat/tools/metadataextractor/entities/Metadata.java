package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.dcm4che3.util.TagUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@ToString
public class Metadata {

    private String fileName;
    private int pos;

    private List<MetaElement> elements = new ArrayList<>();

    // Map of (aaaa,00bb) to privateCreators
    private Map<String, String> privateCreators = new HashMap<>();

    private String studyInstanceUid;

    private String seriesInstanceUid;

    private String sopInstanceUid;

    private String patientId;

    public Metadata(String fileName, int pos) {
        this.fileName = fileName;
        this.pos = pos;
    }

    public void add(MetaElement element) {
        if (element.getValue() == null) {
            element.setValue("");
        }
        int level = element.getLevel();
        if (level == 0) {
            elements.add(element);
        } else {
            MetaElement lastElem = elements.get(elements.size() - 1);
            List<MetaElement> children = lastElem.getChildren();
            for (int i = 0; i < level - 1; i++) {
                lastElem = children.get(children.size() - 1);
                children = lastElem.getChildren();
            }
            if (children == null) {
                children = new ArrayList<>();
                lastElem.setChildren(children);
            }
            children.add(element);
        }
    }
}
