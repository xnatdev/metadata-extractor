package org.nrg.xnat.tools.metadataextractor.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che3.data.*;
import org.dcm4che3.io.DicomInputHandler;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTagDictionary;
import org.nrg.xnat.tools.metadataextractor.entities.MetaElement;
import org.nrg.xnat.tools.metadataextractor.entities.Metadata;
import org.nrg.xnat.tools.metadataextractor.utils.DictionaryUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

@Slf4j
@Getter
public class DicomParser implements DicomInputHandler {
    private final CustomTagDictionary customTagDictionary;
    private Metadata metadata;

    public static final int PIXEL_TAG = 2_145_386_512;
    public static final int STUDY_INSTANCE_UID_TAG = 2_097_165;   // 0020,000D
    public static final int SERIES_INSTANCE_UID_TAG = 2_097_166;  // 0020,000E
    public static final int SOP_INSTANCE_UID_TAG = 524_312; // 0008,0018
    public static final int PATIENT_ID_TAG = 1_048_608; // 0010,0020

    public  DicomParser(CustomTagDictionary customTagDictionary) {
        this.customTagDictionary = customTagDictionary;
    }

    @Override
    public void startDataset(DicomInputStream dis) {
    }

    @Override
    public void endDataset(DicomInputStream dis) {
    }

    @Override
    public void readValue(DicomInputStream dis, Attributes attrs)
            throws IOException {
        VR vr = dis.vr();
        int valLen = dis.length();
        boolean undefLen = valLen == -1;
        int tag = dis.tag();
        String privateCreator = attrs.getPrivateCreator(tag);

        MetaElement element = new MetaElement();
        element.setFileName(metadata.getFileName());
        element.setTag(tag);
        element.setLevel(dis.level());
        element.setPrivateCreator(privateCreator);

        // Skip Pixel data
        if (tag != PIXEL_TAG) {
            metadata.add(element);
        }

        if (vr == VR.SQ || undefLen) {
            element.setKeyword(ElementDictionary.keywordOf(dis.tag(), privateCreator));
            dis.readValue(dis, attrs);
            return;
        }

        byte[] b = dis.readValue();
        StringBuilder line = new StringBuilder();
        vr.prompt(b, dis.bigEndian(),
                attrs.getSpecificCharacterSet(),
                100, line);
        element.setValue(line.toString());

        if (tag == STUDY_INSTANCE_UID_TAG && metadata.getStudyInstanceUid() == null) {
           metadata.setStudyInstanceUid(element.getValue());
        } else if (tag == SERIES_INSTANCE_UID_TAG && metadata.getSeriesInstanceUid() == null) {
            metadata.setSeriesInstanceUid(element.getValue());
        } else if (tag == SOP_INSTANCE_UID_TAG && metadata.getSopInstanceUid() == null) {
            metadata.setSopInstanceUid(element.getValue());
        } else if (tag == PATIENT_ID_TAG && metadata.getPatientId() == null) {
            metadata.setPatientId(element.getValue());
        }

        if (TagUtils.isPrivateCreator(tag)) {
            Map<String, String> privateCreators = metadata.getPrivateCreators();
            privateCreators.put(TagUtils.toString(element.getTag()), element.getValue());
        }

        element.setKeyword(DictionaryUtil.getKeyword(dis.tag(), privateCreator, customTagDictionary));
        if (tag == Tag.FileMetaInformationGroupLength)
            dis.setFileMetaInformationGroupLength(b);
        else if (tag == Tag.TransferSyntaxUID
                || tag == Tag.SpecificCharacterSet
                || TagUtils.isPrivateCreator(tag))
            attrs.setBytes(tag, vr, b);
    }

    @Override
    public void readValue(DicomInputStream dis, Sequence seq)
            throws IOException {
        dis.readValue(dis, seq);
    }

    @Override
    public void readValue(DicomInputStream dis, Fragments frags)
            throws IOException {
        dis.readValue();
    }

    public Metadata parse(Path path, int pos) throws IOException {
        log.debug("Parsing the file: {}", path);
        Metadata metadata = new Metadata(path.toString(), pos);
        this.metadata = metadata;

        try (DicomInputStream dis = new DicomInputStream(path.toFile())) {
            dis.setDicomInputHandler(this);
            dis.readDatasetUntilPixelData();
        }
        log.debug("{} tags found from {}", metadata.getElements().size(), path);
        return metadata;
    }
}
