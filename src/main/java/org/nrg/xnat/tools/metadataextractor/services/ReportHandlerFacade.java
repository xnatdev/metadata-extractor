package org.nrg.xnat.tools.metadataextractor.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.tongfei.progressbar.ProgressBar;
import org.nrg.xnat.tools.metadataextractor.common.Constant;
import org.nrg.xnat.tools.metadataextractor.common.InputArgument;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTagDictionary;
import org.nrg.xnat.tools.metadataextractor.entities.TraversalStat;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportFormat;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportType;
import org.nrg.xnat.tools.metadataextractor.entities.reports.AbstractAnalysisAggregate;
import org.nrg.xnat.tools.metadataextractor.entities.reports.ExampleAnalysisAggregate;
import org.nrg.xnat.tools.metadataextractor.entities.reports.PresentAnalysisAggregate;
import org.nrg.xnat.tools.metadataextractor.entities.reports.SummaryAnalysisAggregate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

@Getter
@Slf4j
public class ReportHandlerFacade {
//    private final Map<ReportType, ReportHandler> reportHandlerMap = new HashMap<>();
    private final Map<ReportType, AbstractAnalysisAggregate> analysisAggregateMap = new HashMap<>();
    private final int numTruncate;
    private final int numFilesPerValue;
    private final List<ReportFormat> reportFormats;
    private final List<ReportType> reportTypes;
    private final CustomTagDictionary customTagDictionary;

    public ReportHandlerFacade(InputArgument inputArgument, CustomTagDictionary customTagDictionary) {
        this.numTruncate = inputArgument.getNumTruncate();
        this.numFilesPerValue = inputArgument.getNumFilesPerValue();
        this.reportFormats = inputArgument.getReportFormats();
        this.reportTypes = inputArgument.getReportTypes();
        this.customTagDictionary = customTagDictionary;

        AbstractAnalysisAggregate analysisAggregate;
        for (ReportType reportType : reportTypes) {
            switch (reportType) {
                case present:
                    analysisAggregate = new PresentAnalysisAggregate(numTruncate);
                    analysisAggregateMap.put(reportType, analysisAggregate);
                    break;

                case summary:
                    analysisAggregate = new SummaryAnalysisAggregate(numTruncate);
                    analysisAggregateMap.put(reportType, analysisAggregate);
                    break;

                case example:
                    analysisAggregate = new ExampleAnalysisAggregate(numTruncate, numFilesPerValue);
                    analysisAggregateMap.put(reportType, analysisAggregate);
                    break;

                default:
                    log.warn("{} is not a supported report type", reportType);
                    break;
            }
        }
    }

    public void writeOverall(File outputDirectory, Aggregator aggregator, List<Path> totalInputPaths, TraversalStat stat, ProgressBar pb) throws IOException {
        for (ReportType reportType : analysisAggregateMap.keySet()) {
            AbstractAnalysisAggregate analysisAggregate = analysisAggregateMap.get(reportType);
            analysisAggregate.constructOverall(aggregator, totalInputPaths);

            for (ReportFormat reportFormat : reportFormats) {
                String path = outputDirectory.getAbsolutePath() +
                        File.separator +
                        analysisAggregate.getFilePrefix() +
                        ReportFormat.getExtension(reportFormat);

                try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
                    pb.step();
                    analysisAggregate.setStat(stat);
                    analysisAggregate.writeOverall(reportFormat, writer);
                }
            }
        }
    }

    public void writeStudy(File outputDirectory, Aggregator aggregator, List<Path> totalInputPaths, ProgressBar pb) throws IOException {
        Map<String, Path> studyFileNameMap = aggregator.getStudyFileNameMap();

        for (String studyInstanceUid : studyFileNameMap.keySet()) {
            Path studyTempFilePath = studyFileNameMap.get(studyInstanceUid);

            for (ReportType reportType : analysisAggregateMap.keySet()) {
                AbstractAnalysisAggregate analysisAggregate = analysisAggregateMap.get(reportType);
                analysisAggregate.constructStudy(studyTempFilePath, aggregator, totalInputPaths);

                for (ReportFormat reportFormat : reportFormats) {
                    String path = outputDirectory.getAbsolutePath() +
                            File.separator +
                            Constant.STUDY_DIRECTORY_NAME +
                            File.separator +
                            studyInstanceUid +
                            "_" +
                            analysisAggregate.getFilePrefix() +
                            ReportFormat.getExtension(reportFormat);

                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
                        pb.step();
                        analysisAggregate.writeStudy(reportFormat, writer);
                    }
                }
            }
        }
    }
}
