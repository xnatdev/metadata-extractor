package org.nrg.xnat.tools.metadataextractor.common;

public class Constant {
    public static final String SYNC_FILE_WRITE = "SYNC_FILE_WRITE";
    public static final String APP_NAME = "metadata-extractor";
    public static final String APP_VERSION = "0.0.7-SNAPSHOT";
    public static final String DEFAULT_REPORT_TYPES = "summary,example,present";
    public static final String DEFAULT_REPORT_FORMATS = "json,text,xml";
    public static final String DEFAULT_INPUT_DIRECTORY = "/input";
    public static final String DEFAULT_OUTPUT_DIRECTORY = "/output";
    public static final String DEFAULT_NUMBER_OF_TRUNCATE = "0";
    public static final String DEFAULT_NUMBER_OF_FILES_PER_VALUE = "1";
    public static final String DEFAULT_NUMBER_OF_THREADS = "1";
    public static final String DEFAULT_UNIQUE_TYPE = "none";
    public static final String[] SUPPORTED_EXTENSIONS = {".dcm", ".DCM"};
    public static final String STUDY_DIRECTORY_NAME = "studies";
    public static final String DEFAULT_DICTIONARY_FILENAME = "default-dictionary.csv";
    public static final String LIST_FILENAME = "list.txt";
    public static final String STAT_FILENAME = "stat.txt";
    public static final Integer CHUNK_COUNT = 10000;
    public static final Integer BUFFER_SIZE = 80 * 1024;

    private Constant() {
    }
}
