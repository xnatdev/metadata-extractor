package org.nrg.xnat.tools.metadataextractor.utils;

import org.dcm4che3.data.ElementDictionary;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTag;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTagDictionary;

import java.util.Optional;

public class TagUtil {
    private TagUtil() {
    }

    public static String getLastTag(String formattedTag) {
        return formattedTag.substring(formattedTag.lastIndexOf('('));
    }

    public static String removeFormatFromTag(String formattedTag) {
        return formattedTag.replace(",", "").substring(1, 9);
    }

    public static int fromTag(String formattedTag) {
        String unformattedTag = removeFormatFromTag(getLastTag(formattedTag));
        return TagUtils.intFromHexString(unformattedTag);
    }
}
