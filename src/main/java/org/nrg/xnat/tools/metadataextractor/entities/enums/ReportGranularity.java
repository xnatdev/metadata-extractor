package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum ReportGranularity {
    overall,
    study
}
