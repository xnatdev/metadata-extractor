package org.nrg.xnat.tools.metadataextractor.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;

import static org.nrg.xnat.tools.metadataextractor.common.Constant.DEFAULT_DICTIONARY_FILENAME;

@Slf4j
public class FileUtil {
    private FileUtil() {
    }

    public static void createDirectory(File directory) throws IOException {
        Files.createDirectories(directory.toPath());
    }

    public static Path createFile(Path directory, String fileName, boolean recreateIfExists) throws IOException {
        Path path = Paths.get(directory.toString(), fileName);
        if (Files.exists(path) && recreateIfExists) {
            Files.delete(path);
        }
        return Files.createFile(path);
    }

    public static Path createTempFile() throws IOException {
        return createTempFile("");
    }

    public static Path createTempFile(String prefix) throws IOException {
        return Files.createTempFile(prefix, ".tmp");
    }

    public static Path createTempFile(Path dir, String prefix) throws IOException {
        return Files.createTempFile(dir, prefix, ".tmp");
    }

    public static Path createTempDirectory() throws IOException {
        return Files.createTempDirectory("");
    }

    public static void deleteTempDirectory(Path dir) throws IOException {
        try (Stream<Path> walk = Files.walk(dir)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(FileUtil::deleteFile);
        }
    }

    public static void deleteFile(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            log.warn("Unable to delete this path {}: {}", path, e);
        }
    }

    public static File loadResourceFile(String fileName) throws IOException {
        Path path = FileUtil.createTempFile();
        try (
                InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(fileName);
                OutputStream outStream = new FileOutputStream(path.toFile());
        ) {
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        }
        return path.toFile();
    }
}
