package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum UniqueType {
    none,
    series,
    study
}
