package org.nrg.xnat.tools.metadataextractor.entities.reports;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.entities.AggregateUnit;
import org.nrg.xnat.tools.metadataextractor.entities.AggregatorRecord;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportType;
import org.nrg.xnat.tools.metadataextractor.exceptions.AggregatorRecordParseException;
import org.nrg.xnat.tools.metadataextractor.services.Aggregator;
import org.nrg.xnat.tools.metadataextractor.utils.JsonUtil;
import org.nrg.xnat.tools.metadataextractor.utils.TagUtil;
import org.nrg.xnat.tools.metadataextractor.utils.XmlUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Setter
@Getter
public class SummaryAnalysisAggregate extends AbstractAnalysisAggregate {
    static ExclusionStrategy overallExclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            return field.getName().equals("numTruncate");
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    static ExclusionStrategy studyExclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            return field.getName().equals("numTruncate") || field.getName().equals("stat") || field.getName().equals("aggregator") || field.getName().equals("totalInputPaths");
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    @JsonIgnore
    Aggregator aggregator;

    @JsonIgnore
    List<Path> totalInputPaths;

    @JacksonXmlElementWrapper()
    private Map<String, Counter> standardElements = new TreeMap<>();

    @JacksonXmlElementWrapper()
    private Map<String, Counter> privateElements = new TreeMap<>();

    public SummaryAnalysisAggregate(int numTruncate) {
        super(numTruncate);
    }

    @Override
    public void clear() {
        standardElements.clear();
        privateElements.clear();
    }

    @Override
    public void writeOverallText(Writer writer) throws IOException {
        writeStat(writer);

        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            constructTag(aggregator, totalInputPaths, formattedTag);
            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                formatElementsToText(privateElements, writer);
            } else {
                formatElementsToText(standardElements, writer);
            }
        }
    }

    private void formatElementsToText(Map<String, SummaryAnalysisAggregate.Counter> elements, Writer writer) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            Counter counter = elements.get(tag);
            Map<String, Integer> counterMap = counter.getCounters();

            writeTextLine(writer);
            writer.write(String.format("%s\t%s\n", tag, counter.getKeyword()));
            writeTextLine(writer);
            List<String> counterKeys = new ArrayList<>(counterMap.keySet());
            Collections.sort(counterKeys);

            int count = 0;
            for (String counterKey : counterKeys) {
                writer.write(String.format("%s [Frequency: %d]\n", counterKey, counterMap.get(counterKey)));
//                count++;
//                if (numTruncate > 0 && count >= numTruncate) {
//                    break;
//                }
            }
            writer.write('\n');
        }
    }

    @Override
    public void writeOverallJson(Writer writer) throws IOException {
        writer.write("{");

        // standardElements
        writer.write("\"standardElements\":{");
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        int count = 0;
        writeJson(writer, tagFileNameMap, count, standardElements);
        writer.write("},");

        // privateElements
        writer.write("\"privateElements\":{");
        count = 0;
        writeJson(writer, tagFileNameMap, count, privateElements);
        writer.write("},");

        // stat
        writer.write(String.format("\"stat\":{\"numStudyInstanceUids\":%d,\"numSeriesInstaneUids\":%d,\"numSopInstanceUids\":%d,\"numPatientIds\":%d}", stat.getNumStudyInstanceUids(), stat.getNumSeriesInstaneUids(), stat.getNumSopInstanceUids(), stat.getNumPatientIds()));
        writer.write("}");
    }

    private void writeJson(Writer writer, Map<String, Path> tagFileNameMap, int count, Map<String, Counter> elements) throws IOException {
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                if (elements != privateElements) {
                    continue;
                }
            } else {
                if (elements != standardElements) {
                    continue;
                }
            }

            constructTag(aggregator, totalInputPaths, formattedTag);
            if (elements.size() == 0) {
                continue;
            }
            if (count > 0) {
                writer.write(",");
            }
            JsonUtil.write(elements, writer);

            count++;
        }
    }

    @Override
    public void writeOverallXml(Writer writer) throws IOException {
        writer.write("<" + this.getClass().getSimpleName() + ">\n");

        // stat
        writer.write("  <stat>\n");
        writer.write("    <numStudyInstanceUids>");
        writer.write(Integer.toString(stat.getNumStudyInstanceUids()));
        writer.write("</numStudyInstanceUids>\n");

        writer.write("    <numSeriesInstaneUids>");
        writer.write(Integer.toString(stat.getNumSeriesInstaneUids()));
        writer.write("</numSeriesInstaneUids>\n");

        writer.write("    <numSopInstanceUids>");
        writer.write(Integer.toString(stat.getNumSopInstanceUids()));
        writer.write("</numSopInstanceUids>\n");

        writer.write("    <numPatientIds>");
        writer.write(Integer.toString(stat.getNumPatientIds()));
        writer.write("</numPatientIds>\n");
        writer.write("  </stat>\n");

        // standardElements
        writer.write("  <standardElements>\n");
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        int count = 0;
        writeXml(writer, tagFileNameMap, count, standardElements);
        writer.write("  </standardElements>\n");

        // privateElements
        writer.write("  <privateElements>\n");
        count = 0;
        writeXml(writer, tagFileNameMap, count, privateElements);
        writer.write("  </privateElements>\n");

        writer.write("</" + this.getClass().getSimpleName() + ">\n");
    }

    private void writeXml(Writer writer, Map<String, Path> tagFileNameMap, int count, Map<String, SummaryAnalysisAggregate.Counter> elements) throws IOException {
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                if (elements != privateElements) {
                    continue;
                }
            } else {
                if (elements != standardElements) {
                    continue;
                }
            }

            constructTag(aggregator, totalInputPaths, formattedTag);
            if (elements.size() == 0) {
                continue;
            }
            XmlUtil.write(elements, writer);

            count++;
        }
    }

    @Override
    public void writeStudyText(Writer writer) throws IOException {
        formatElementsToText(standardElements, writer);
        formatElementsToText(privateElements, writer);
    }

    @Override
    public void writeStudyJson(Writer writer) {
        JsonWriter jsonWriter = new JsonWriter(writer);
        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(studyExclusionStrategy).setPrettyPrinting().create();
        gson.toJson(this, SummaryAnalysisAggregate.class, jsonWriter);
    }

    @Override
    public void writeStudyXml(Writer writer) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.writeValue(writer, this);
    }

    //    @Override
//    public void constructOverall(Aggregator aggregator, List<Path> totalInputPaths) throws IOException {
//        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
//
//        boolean isPrivate;
//        String keyword;
//        Map<String, Counter> map;
//        AggregatorRecord rec;
//        Counter valueInfo;
//
//        for (String formattedTag : tagFileNameMap.keySet()) {
//            Path tagFilePath = tagFileNameMap.get(formattedTag);
//            try (
//                    FileReader fw = new FileReader(tagFilePath.toFile());
//                    BufferedReader bw = new BufferedReader(fw)
//            ) {
//                bw.readLine();  // Skip formattedTag
//                isPrivate = Boolean.valueOf(bw.readLine());
//                keyword = bw.readLine();
//                map = isPrivate ? privateElements : standardElements;
//                for (String line; (line = bw.readLine()) != null; ) {
//                    try {
//                        rec = AggregatorRecord.toObject(line, totalInputPaths);
//                    } catch (AggregatorRecordParseException e) {
//                        log.warn("Invalid record: {} at {}", line, tagFilePath);
//                        continue;
//                    }
//
//                    if (!map.containsKey(formattedTag)) {
//                        valueInfo = new Counter(keyword);
//                        map.put(formattedTag, valueInfo);
//                    } else {
//                        valueInfo = map.get(formattedTag);
//                    }
//
//                    if (!valueInfo.getCounters().containsKey(rec.getValue())) {
//                        valueInfo.getCounters().put(rec.getValue(), 1);
//                    } else {
//                        valueInfo.getCounters().put(rec.getValue(), valueInfo.getCounters().get(rec.getValue()) + 1);
//                    }
//                }
//            }
//        }
//    }
    @Override
    public void constructOverall(Aggregator aggregator, List<Path> totalInputPaths) {
        this.aggregator = aggregator;
        this.totalInputPaths = totalInputPaths;
    }

    @Override
    public void constructStudy(Path studyTempFilePath, Aggregator aggregator, List<Path> totalInputPaths) throws IOException {
        clear();

        try (
                FileReader fw = new FileReader(studyTempFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw)
        ) {
            bw.readLine();  // studyInstanceUid

            AggregatorRecord rec;
            Map<String, Counter> map;
            Boolean isPrivate;
            String keyword;
            String formattedTag;
            Counter valueInfo;

            for (String line; (line = bw.readLine()) != null; ) {
                try {
                    rec = AggregatorRecord.toObject(line, totalInputPaths);
                } catch (AggregatorRecordParseException e) {
                    log.warn("Invalid record: {} at {}", line, studyTempFilePath);
                    continue;
                }

                isPrivate = rec.getIsPrivate();
                formattedTag = rec.getFormattedTag();
                keyword = rec.getKeyword();

                map = isPrivate ? privateElements : standardElements;

                if (!map.containsKey(formattedTag)) {
                    valueInfo = new Counter(keyword);
                    map.put(formattedTag, valueInfo);
                } else {
                    valueInfo = map.get(formattedTag);
                }

                if (!valueInfo.getCounters().containsKey(rec.getValue())) {
                    valueInfo.getCounters().put(rec.getValue(), 1);
                } else {
                    valueInfo.getCounters().put(rec.getValue(), valueInfo.getCounters().get(rec.getValue()) + 1);
                }
            }
        }
    }

    public void constructTag(Aggregator aggregator, List<Path> totalInputPaths, String formattedTag) throws IOException {
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();

        boolean isPrivate;
        String keyword;
        Map<String, Counter> map;
        AggregatorRecord rec;
        Counter valueInfo;

        Path tagFilePath = tagFileNameMap.get(formattedTag);
        try (
                FileReader fw = new FileReader(tagFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw);
        ) {
            bw.readLine();  // Skip formattedTag
            isPrivate = Boolean.valueOf(bw.readLine());
            keyword = bw.readLine();
            map = isPrivate ? privateElements : standardElements;
            for (String line; (line = bw.readLine()) != null; ) {
                try {
                    rec = AggregatorRecord.toObject(line, totalInputPaths);
                } catch (AggregatorRecordParseException e) {
                    log.warn("Invalid record: {} at {}, {}", line, tagFilePath, e.getMessage());
                    continue;
                }

                if (!map.containsKey(formattedTag)) {
                    valueInfo = new Counter(keyword);
                    map.put(formattedTag, valueInfo);
                } else {
                    valueInfo = map.get(formattedTag);
                }

                if (!valueInfo.getCounters().containsKey(rec.getValue())) {
                    valueInfo.getCounters().put(rec.getValue(), 1);
                } else {
                    valueInfo.getCounters().put(rec.getValue(), valueInfo.getCounters().get(rec.getValue()) + 1);
                }
            }
        }
    }

    @Override
    public String getFilePrefix() {
        return ReportType.summary.toString();
    }

    @Getter
    static public class Counter implements AggregateUnit {
        private final String keyword;

        private final Map<String, Integer> counters = new TreeMap<>();

        public Counter(String keyword) {
            this.keyword = keyword;
        }
    }
}
