package org.nrg.xnat.tools.metadataextractor;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.tools.metadataextractor.common.InputArgument;
import org.nrg.xnat.tools.metadataextractor.entities.TraversalStat;
import org.nrg.xnat.tools.metadataextractor.services.Traversal;
import picocli.CommandLine;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@Slf4j
class Main {
    public static void main(String... args) throws IOException, InterruptedException {
        log.debug("Starting the metadata-extractor");
        new Main().start(args);
    }

    public InputArgument parseArguments(String... args) throws IllegalArgumentException, InterruptedException {
        log.debug("Parsing input arguments");
        InputArgument inputArgument = new InputArgument();

        CommandLine commandLine = new CommandLine(inputArgument);
        int exitCode = commandLine.execute(args);

        if (commandLine.isUsageHelpRequested()) {
            log.debug("Help parameter detected, the process will be terminated");
            throw new InterruptedException();
        }
        if (commandLine.isVersionHelpRequested()) {
            log.debug("Version parameter detected, the process will be terminated");
            throw new InterruptedException();
        }

        if (exitCode > 0) {
            throw new IllegalArgumentException();
        }

        return inputArgument;
    }

    public void start(String... args) throws IOException, InterruptedException {
        InputArgument inputArgument = null;
        try {
            inputArgument = parseArguments(args);
        } catch (IllegalArgumentException e) {
            System.exit(1);
        } catch (InterruptedException e) {
            System.exit(0);
        }

        Traversal traversal = new Traversal(inputArgument);

        // Perform a pre process
        traversal.preProcess();

        // Retrieve DICOM files to process
        List<Path> totalInputPaths;
        if (inputArgument.getDicomListFile() == null) {
            totalInputPaths = traversal.retrieveFiles();
        } else {
            totalInputPaths = traversal.loadFilePaths(inputArgument.getDicomListFile());
            System.out.println("Skipping to walk through dicom files");
            log.debug("Skipping to walk through dicom files");
        }

        // Aggregate metadata from DICOm files
        TraversalStat stat = null;
        if (!inputArgument.getWriteOnly()) {
            traversal.processAlDirectories(totalInputPaths);
            stat = traversal.getStat();
            traversal.keepStat(stat);
            traversal.clearStatistics();
            System.out.println();
        } else {
            System.out.println("Skipping to process DICOM files");
            stat = traversal.loadStat();
        }

        // Write overall reports
        System.out.println("Writing result files");
        traversal.writeOverallFiles(totalInputPaths, stat);

        if (inputArgument.getStudyReportProduced()) {
            // Write study reports
            traversal.writeStudyFiles(totalInputPaths);
        }

        // Perform a post process
        traversal.postProcess();
    }
}