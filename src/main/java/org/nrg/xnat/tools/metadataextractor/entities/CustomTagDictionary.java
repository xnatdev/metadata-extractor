package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che3.data.VR;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Slf4j
public class CustomTagDictionary {
    private final Map<String, Map<VR, CustomTag>> dictionary = new HashMap<>();

    public void add(List<CustomTag> customTagGroup) {
        String formattedTag = customTagGroup.stream().map(CustomTag::getFormattedTag).collect(Collectors.joining());
        CustomTag lastCustomTag = customTagGroup.get(customTagGroup.size() - 1);

        if (!dictionary.containsKey(formattedTag)) {
            Map<VR, CustomTag> customTagMap = new HashMap<>();
            dictionary.put(formattedTag, customTagMap);
        }
        dictionary.get(formattedTag).put(lastCustomTag.getVr(), lastCustomTag);
    }


    public Optional<CustomTag> search(String formattedTag) {
        return search(formattedTag, null);
    }

    public Optional<CustomTag> search(String formattedTag, VR vr) {
        Map<VR, CustomTag> customTagMap = dictionary.get(formattedTag);
        if (customTagMap == null) {
            return Optional.empty();
        }
        if (vr == null) {
            if (customTagMap.keySet().size() == 0) {
                return Optional.empty();
            }
            return Optional.of(customTagMap.get(customTagMap.keySet().toArray()[0]));
        }
        if (!customTagMap.containsKey(vr)) {
            return Optional.empty();
        }
        return Optional.of(customTagMap.get(vr));
    }

    public void construct(File dictionaryFile) throws IOException {
        try (FileReader fileReader = new FileReader(dictionaryFile);
             BufferedReader reader = new BufferedReader(fileReader)) {

            int lineCnt = 0, procCnt = 0;
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                lineCnt++;
                try {
                    List<CustomTag> customTagGroup = parseCustomTag(line);
                    add(customTagGroup);
                    procCnt++;
                } catch(IllegalArgumentException ignored) {}
            }
            log.debug("{} lines read from {}, {} tags acquired", lineCnt, dictionaryFile.getAbsolutePath(), procCnt);
        }
    }

    private List<CustomTag> parseCustomTag(String line) throws IllegalArgumentException {
        List<CustomTag> customTagGroups = new ArrayList<>();
        if(line == null || line.length() == 0 || line.charAt(0) != '"') {
            throw new IllegalArgumentException("line doesn't begin with \"");
        }
        if (line.chars().filter(ch -> ch == ',').count() < 4) {
            throw new IllegalArgumentException("# of coulmns not sufficient");
        }
        // skip  private_disposition column
        int pos = line.lastIndexOf(',', line.lastIndexOf(',', line.lastIndexOf(',', line.lastIndexOf(',') - 1) - 1) - 1);
        String[] tokens = line.substring(pos).split(",");
        String vr = tokens[1].replaceAll("\"", "");
        String value = tokens[2].replaceAll("\"", "");
        String rawTag = line.substring(1, pos - 1);

        String[] rawTagTokens = rawTag.split("\\[<[0-9]+>]");

        for (String rawTagToken : rawTagTokens) {
            CustomTag customTag = new CustomTag(rawTagToken, VR.valueOf(vr), value);
            customTagGroups.add(customTag);
        }
        return customTagGroups;
    }
}
