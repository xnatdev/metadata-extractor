package org.nrg.xnat.tools.metadataextractor.utils;

import org.nrg.xnat.tools.metadataextractor.entities.reports.ExampleAnalysisAggregate;
import org.nrg.xnat.tools.metadataextractor.entities.reports.SummaryAnalysisAggregate;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class XmlUtil {
    private XmlUtil() {
    }

    private static String escape(String str) {
        return str.replaceAll("<", "\\<");
    }

    public static void write(Map<String, ExampleAnalysisAggregate.ValueInfo> elements, Writer writer, int numFilesPerValue, int numTruncate) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            writer.write("    <");
            writer.write(escape(tag));
            writer.write(">\n");

            ExampleAnalysisAggregate.ValueInfo valueInfo = elements.get(tag);

            writer.write("      <keyword>");
            writer.write(escape(valueInfo.getKeyword()));
            writer.write("</keyword>\n");

            Map<String, List<String>> fileNames = valueInfo.getFileNames();
            List<String> fileNamesKeys = new ArrayList<>(fileNames.keySet());
            Collections.sort(fileNamesKeys);

            writer.write("      <fileNames>\n");
            int valueCount = numTruncate <= 0 ? fileNamesKeys.size() : Math.min(numTruncate, fileNamesKeys.size());
            for (int i = 0; i < valueCount; i++) {
                List<String> names = fileNames.get(fileNamesKeys.get(i));
                Collections.sort(names);

                // When numFilesPerValue == 0, don't display file names
                if (numFilesPerValue != 0) {
                    int count = numFilesPerValue == 0 ? names.size() : Math.min(numFilesPerValue, names.size());
                    for (int j = 0; j < count; j++) {
                        writer.write("        <");
                        writer.write(escape(fileNamesKeys.get(i)));
                        writer.write(">");
                        writer.write(escape(names.get(j)));
                        writer.write("</");
                        writer.write(escape(fileNamesKeys.get(i)));
                        writer.write(">\n");
                    }
                }
            }

            writer.write("      </fileNames>\n");
            writer.write("    </");
            writer.write(escape(tag));
            writer.write(">\n");
        }
    }

    public static void write(Map<String, SummaryAnalysisAggregate.Counter> elements, Writer writer) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            writer.write("    <");
            writer.write(escape(tag));
            writer.write(">\n");

            SummaryAnalysisAggregate.Counter valueInfo = elements.get(tag);

            writer.write("      <keyword>");
            writer.write(escape(valueInfo.getKeyword()));
            writer.write("</keyword>\n");

            writer.write("      <counters>\n");
            Map<String, Integer> counters = valueInfo.getCounters();
            for (String value : counters.keySet()) {
                Integer count = counters.get(value);

                writer.write("        <");
                writer.write(escape(value));
                writer.write(">");
                writer.write(Integer.toString(count));
                writer.write("</");
                writer.write(escape(value));
                writer.write(">\n");
            }

            writer.write("      </counters>\n");
            writer.write("    </");
            writer.write(escape(tag));
            writer.write(">\n");
        }
    }
}
