package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum ReportOption {
    present,
    value,
    example,
    truncate
}
