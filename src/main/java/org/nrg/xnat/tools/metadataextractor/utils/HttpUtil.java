package org.nrg.xnat.tools.metadataextractor.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

@Slf4j
public class HttpUtil {
    private HttpUtil() {
    }

    public static File download(URL url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        Path path;

        try {
            log.debug("Retrieving a dictionary file from {} via HTTP", url.toString());
            HttpGet request = new HttpGet(url.toString());
            path = FileUtil.createTempFile();

            try (
                    CloseableHttpResponse response = httpClient.execute(request);
                    FileWriter writer = new FileWriter(path.toFile());
                    BufferedWriter bufferedWriter = new BufferedWriter(writer);
            ) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    log.debug("Saving the dictionary file to {}", path);
                    // return it as a String
                    String result = EntityUtils.toString(entity);
                    bufferedWriter.write(result);
                } else {
                    log.warn("No dictionary file");
                }
            }
        } finally {
            httpClient.close();
        }

        return path.toFile();
    }
}
