package org.nrg.xnat.tools.metadataextractor.entities.reports;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonWriter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.tools.metadataextractor.entities.AggregateUnit;
import org.nrg.xnat.tools.metadataextractor.entities.AggregatorRecord;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportType;
import org.nrg.xnat.tools.metadataextractor.exceptions.AggregatorRecordParseException;
import org.nrg.xnat.tools.metadataextractor.services.Aggregator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Setter
@Getter
public class PresentAnalysisAggregate extends AbstractAnalysisAggregate {
    static ExclusionStrategy overallExclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            return field.getName().equals("numTruncate");
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    static ExclusionStrategy studyExclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            return field.getName().equals("numTruncate") || field.getName().equals("stat");
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    @Expose
    @JacksonXmlElementWrapper()
    private Map<String, Counter> standardElements = new TreeMap<>();

    @Expose
    @JacksonXmlElementWrapper()
    private Map<String, Counter> privateElements = new TreeMap<>();

    public PresentAnalysisAggregate(int numTruncate) {
        super(numTruncate);
    }

    @Override
    public void clear() {
        standardElements.clear();
        privateElements.clear();
    }

//    @Override
//    public void add(MetaElement element, List<String> parentTags, Map<String, String> privateCreators) {
//        int tag = element.getTag();
//        Map<String, Counter> map = TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag) ? privateElements : standardElements;
//        add(element, parentTags, privateCreators, map);
//    }
//
//    private void add(MetaElement element, List<String> parentTags, Map<String, String> privateCreators, Map<String, Counter> map) {
//        String formattedTag =
//                (parentTags != null ? parentTags.stream().collect(Collectors.joining("")) : "")
//                        + TagUtils.toString(element.getTag());
//
//        synchronized (this) {
//            if (!map.containsKey(formattedTag)) {
//                map.put(formattedTag, new Counter(element.getKeyword()));
//            }
//            map.get(formattedTag).setFrequency(map.get(formattedTag).getFrequency() + 1);
//        }
//    }

    private void formatElementsToText(Map<String, Counter> elements, Writer writer) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            Counter counter = elements.get(tag);
            writer.write(String.format("%s [%s] [Frequency: %d]\n", tag, counter.getKeyword(), counter.getFrequency()));
        }
    }

    @Override
    public void writeOverallText(Writer writer) throws IOException {
        writeStat(writer);

        formatElementsToText(standardElements, writer);
        formatElementsToText(privateElements, writer);
    }

    @Override
    public void writeOverallJson(Writer writer) {
        JsonWriter jsonWriter = new JsonWriter(writer);
        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(overallExclusionStrategy).setPrettyPrinting().create();
        gson.toJson(this, PresentAnalysisAggregate.class, jsonWriter);
    }

    @Override
    public void writeOverallXml(Writer writer) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.writeValue(writer, this);
    }

    @Override
    public void writeStudyText(Writer writer) throws IOException {
        formatElementsToText(standardElements, writer);
        formatElementsToText(privateElements, writer);
    }

    @Override
    public void writeStudyJson(Writer writer) {
        JsonWriter jsonWriter = new JsonWriter(writer);
        Gson gson = new GsonBuilder().addSerializationExclusionStrategy(studyExclusionStrategy).setPrettyPrinting().create();
        gson.toJson(this, PresentAnalysisAggregate.class, jsonWriter);
    }

    @Override
    public void writeStudyXml(Writer writer) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.writeValue(writer, this);
    }

    @Override
    public void constructOverall(Aggregator aggregator, List<Path> totalInputPaths) throws IOException {
        clear();

        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();

        boolean isPrivate;
        String keyword;
        Map<String, Counter> map;
        Counter valueInfo;

        for (String formattedTag : tagFileNameMap.keySet()) {
            Path tagFilePath = tagFileNameMap.get(formattedTag);
            try (
                    FileReader fw = new FileReader(tagFilePath.toFile());
                    BufferedReader bw = new BufferedReader(fw)
            ) {
                bw.readLine();  // Skip formattedTag
                isPrivate = Boolean.valueOf(bw.readLine());
                keyword = bw.readLine();
                map = isPrivate ? privateElements : standardElements;

                for (String line; (line = bw.readLine()) != null; ) {
                    try {
                        AggregatorRecord.toObject(line, totalInputPaths);
                    } catch (AggregatorRecordParseException e) {
                        log.warn("Invalid record: {} at {}", line, tagFilePath);
                        continue;
                    }

                    if (!map.containsKey(formattedTag)) {
                        valueInfo = new Counter(keyword);
                        map.put(formattedTag, valueInfo);
                    } else {
                        valueInfo = map.get(formattedTag);
                    }

                    valueInfo.setFrequency(valueInfo.getFrequency() + 1);
                }
            }
        }
    }

    @Override
    public void constructStudy(Path studyTempFilePath, Aggregator aggregator, List<Path> totalInputPaths) throws IOException {
        clear();

        try (
                FileReader fw = new FileReader(studyTempFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw)
        ) {
            bw.readLine();  // studyInstanceUid

            AggregatorRecord rec;
            Map<String, Counter> map;
            Boolean isPrivate;
            String keyword;
            String formattedTag;
            Counter valueInfo;

            for (String line; (line = bw.readLine()) != null; ) {
                try {
                    rec = AggregatorRecord.toObject(line, totalInputPaths);
                } catch (AggregatorRecordParseException e) {
                    log.warn("Invalid record: {} at {}", line, studyTempFilePath);
                    continue;
                }

                isPrivate = rec.getIsPrivate();
                formattedTag = rec.getFormattedTag();
                keyword = rec.getKeyword();

                map = isPrivate ? privateElements : standardElements;

                if (!map.containsKey(formattedTag)) {
                    valueInfo = new Counter(keyword);
                    map.put(formattedTag, valueInfo);
                } else {
                    valueInfo = map.get(formattedTag);
                }

                valueInfo.setFrequency(valueInfo.getFrequency() + 1);
            }
        }
    }

    @Override
    public String getFilePrefix() {
        return ReportType.present.toString();
    }

    @Getter
    @Setter
    static public class Counter implements AggregateUnit {
        private String keyword;

        private int frequency = 1;

        public Counter(String keyword) {
            this.keyword = keyword;
        }
    }
}
