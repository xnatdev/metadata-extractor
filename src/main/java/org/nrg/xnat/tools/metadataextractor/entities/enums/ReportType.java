package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum ReportType {
    present,
    summary,
    example
}
