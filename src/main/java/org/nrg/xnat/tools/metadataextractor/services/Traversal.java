package org.nrg.xnat.tools.metadataextractor.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import org.nrg.xnat.tools.metadataextractor.common.Constant;
import org.nrg.xnat.tools.metadataextractor.common.InputArgument;
import org.nrg.xnat.tools.metadataextractor.entities.*;
import org.nrg.xnat.tools.metadataextractor.entities.enums.UniqueType;
import org.nrg.xnat.tools.metadataextractor.utils.FileUtil;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;

@Slf4j
@Getter
public class Traversal {
    private final Set<String> studyInstanceUids = new HashSet<>();
    private final Set<String> seriesInstanceUids = new HashSet<>();
    private final Set<String> sopInstanceUids = new HashSet<>();
    private final Set<String> patientIds = new HashSet<>();
    private final CustomTagDictionary customTagDictionary = new CustomTagDictionary();

    private final InputArgument inputArgument;
    private ReportHandlerFacade handlerFacade;
    private Aggregator aggregator;

    public Traversal(InputArgument inputArgument) {
        this.inputArgument = inputArgument;
    }

    public static Path getStudyOutputPath(File outputDirectory) {
        return Paths.get(outputDirectory.toString(), Constant.STUDY_DIRECTORY_NAME);
    }

    public TraversalStat getStat() {
        return new TraversalStat(
                this.getStudyInstanceUids().size(),
                this.getSeriesInstanceUids().size(),
                this.getSopInstanceUids().size(),
                this.getPatientIds().size()
        );
    }

    public void clearStatistics() {
        studyInstanceUids.clear();
        seriesInstanceUids.clear();
        sopInstanceUids.clear();
        patientIds.clear();
    }

    public void preProcess() throws IOException {
        File outputDirectory = inputArgument.getOutputDirectory();
        log.debug("Checking the output directory: {}", outputDirectory.getCanonicalPath());
        if (!Files.exists(outputDirectory.toPath())) {
            log.debug("Creating the output directory: {}", outputDirectory.getCanonicalPath());
            FileUtil.createDirectory(outputDirectory);
        }
        if (inputArgument.getStudyReportProduced()) {
            Path studiesPath = Traversal.getStudyOutputPath(outputDirectory);
            if (!Files.exists(studiesPath)) {
                log.debug("Creating the studies directory: {}", outputDirectory.getCanonicalPath());
                FileUtil.createDirectory(studiesPath.toFile());
            }
        }

        File intermediateDirectory = inputArgument.getIntermediateDirectory();
        if (intermediateDirectory != null) {
            log.debug("Checking the intermediate directory: {}", intermediateDirectory.getCanonicalPath());
            if (!Files.exists(intermediateDirectory.toPath())) {
                log.debug("Creating the intermediate directory: {}", intermediateDirectory.getCanonicalPath());
                FileUtil.createDirectory(intermediateDirectory);
            }
        }

        // Construct dictionary
        for (File dictionaryFile : inputArgument.getDictionaryFiles()) {
            log.debug("Constructing dictionary files using {}", dictionaryFile);
            customTagDictionary.construct(dictionaryFile);
        }

        this.handlerFacade = new ReportHandlerFacade(
                inputArgument,
                customTagDictionary
        );

        this.aggregator = new Aggregator(inputArgument);
    }

    public void postProcess() throws IOException {
        log.debug("Performing the post-process");
        aggregator.postProcess();
    }

    public List<Path> retrieveFiles() throws IOException {
        log.debug("Retrieving input files");
        List<File> inputDirectories = inputArgument.getInputDirectories();
        List<Path> totalInputPaths = new ArrayList<>();

        try (ProgressBar pb = new ProgressBar("Retrieving DICOM files", 1)) { // name, initial max
            for (File inputDirectory : inputDirectories) {
                log.debug("Beginning retrieving the input directory {}", inputDirectory.getAbsolutePath());
                List<Path> inputPaths = processDirectory(inputDirectory, pb);
                log.debug("{} files found from the input directory {}", inputPaths.size(), inputDirectory.getAbsolutePath());

                totalInputPaths.addAll(inputPaths);
            }
        }

        log.debug("{} DICOM files found", totalInputPaths.size());
        System.out.printf("%d DICOM files found%n", totalInputPaths.size());

        // Write retrieved file list to a file
        writeRetrievedFileList(totalInputPaths);

        return totalInputPaths;
    }

    public List<Path> loadFilePaths(File dicomListFile) throws IOException {
        log.debug("Loading DICOM file list");
        List<Path> totalInputPaths = new ArrayList<>();

        try (
                FileReader fw = new FileReader(dicomListFile);
                BufferedReader bw = new BufferedReader(fw)
        ) {
            for (String line; (line = bw.readLine()) != null; ) {
                totalInputPaths.add(Paths.get(line));
            }
        }

        return totalInputPaths;
    }

    private void writeRetrievedFileList(List<Path> totalInputPath) throws IOException {
        Path tempDirectoryPath = aggregator.getTempDirectoryPath();
        Path listFilePath = FileUtil.createFile(tempDirectoryPath, Constant.LIST_FILENAME, true);
        try (
                FileWriter fw = new FileWriter(listFilePath.toString(), true);
                BufferedWriter bw = new BufferedWriter(fw)
        ) {
            for (Path path : totalInputPath) {
                bw.write(path.toString());
                bw.newLine();
            }
        }
    }

    private void processFile(Path inputPath, int pos) {
        UniqueType uniqueType = inputArgument.getUniqueType();

        if (!Files.exists(inputPath)) {
            log.warn("File doesn't exist: ",
                    inputPath);
            return;
        }

        // Parse a metadata
        Metadata metadata;
        try {
            DicomParser parser = new DicomParser(customTagDictionary);
            metadata = parser.parse(inputPath, pos);
        } catch (IOException e) {
            log.warn("{}: {}", e.toString(), inputPath);
            return;
        }

        // Check unique types
        if (uniqueType == UniqueType.study) {
            String studyInstanceUid = metadata.getStudyInstanceUid();
            if (studyInstanceUid != null && studyInstanceUids.contains(studyInstanceUid)) {
                log.debug("Skip the already processed study: {}", studyInstanceUid);
                return;
            }
        } else if (uniqueType == UniqueType.series) {
            String seriesInstanceUid = metadata.getSeriesInstanceUid();
            if (seriesInstanceUid != null && seriesInstanceUids.contains(seriesInstanceUid)) {
                log.debug("Skip the already processed series: {}", seriesInstanceUid);
                return;
            }

        }

        studyInstanceUids.add(metadata.getStudyInstanceUid());
        seriesInstanceUids.add(metadata.getSeriesInstanceUid());
        sopInstanceUids.add(metadata.getSopInstanceUid());
        patientIds.add(metadata.getPatientId());

        try {
            aggregator.aggregate(metadata);
        } catch (IOException e) {
            log.warn("Failed to aggregate metadata for {}: {}", inputPath, e.toString());
        }
    }

    public void processAlDirectories(List<Path> totalInputPaths) throws IOException, InterruptedException {
        System.out.println("# threads: " + inputArgument.getNumThreads());
        ExecutorService executor = Executors.newFixedThreadPool(inputArgument.getNumThreads());

        ProgressBarBuilder pbb = new ProgressBarBuilder()
                .setInitialMax(totalInputPaths.size())
                .setTaskName("Processing DICOM files");
        ProgressBar pb = pbb.build();

        int pos = -1;
        for (Path inputPath : totalInputPaths) {
            pos++;
            int finalPos = pos;
            executor.execute(() -> {
                processFile(inputPath, finalPos);
                synchronized (pb) {
                    pb.step();
                }
            });
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aggregator.appendPermanently();
        Thread.sleep(1000);
    }

    public void writeOverallFiles(List<Path> totalInputPaths, TraversalStat stat) throws IOException {
        log.debug("Writing overall report files");
        try (ProgressBar pb = new ProgressBar("Building overall reports",
                (long) inputArgument.getReportFormats().size() * inputArgument.getReportTypes().size())) {
            handlerFacade.writeOverall(inputArgument.getOutputDirectory(), aggregator, totalInputPaths, stat, pb);
        }
    }

    public void writeStudyFiles(List<Path> totalInputPaths) throws IOException {
        log.debug("Writing study report files");
        try (ProgressBar pb = new ProgressBar("Building study reports", (long) aggregator.getStudyFileNameMap().keySet().size() *
                inputArgument.getReportFormats().size() * inputArgument.getReportTypes().size())
        ) {
            handlerFacade.writeStudy(inputArgument.getOutputDirectory(), aggregator, totalInputPaths, pb);
        }
    }

    private boolean isSupportedFile(Path path) {
        for (int i = 0; i < Constant.SUPPORTED_EXTENSIONS.length; i++) {
            if (path.toString().endsWith(Constant.SUPPORTED_EXTENSIONS[i])) {
                return true;
            }
        }
        return false;
    }

    public List<Path> processDirectory(File inputDirectory, ProgressBar pb) throws IOException {
        List<Path> inputPaths = new ArrayList<>();
        Path inputPath = inputDirectory.toPath();
        Files.walkFileTree(inputPath, EnumSet.of(FOLLOW_LINKS), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
                pb.step(); // step by 1
                if (isSupportedFile(path)) {
                    log.debug("dcm found: {}", path);
                    inputPaths.add(path);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                // This is important to note. Test this behaviour
                return FileVisitResult.CONTINUE;
            }
        });
        return inputPaths;
    }

    public void keepStat(TraversalStat stat) throws IOException {
        log.debug("Keeping stats");

        Path tempDirectoryPath = aggregator.getTempDirectoryPath();
        Path statFilePath = FileUtil.createFile(tempDirectoryPath, Constant.STAT_FILENAME, true);
        try (
                FileWriter fw = new FileWriter(statFilePath.toString(), true);
                BufferedWriter bw = new BufferedWriter(fw)
        ) {
            bw.write(Integer.toString(stat.getNumStudyInstanceUids()));
            bw.write('\n');
            bw.write(Integer.toString(stat.getNumSeriesInstaneUids()));
            bw.write('\n');
            bw.write(Integer.toString(stat.getNumSopInstanceUids()));
            bw.write('\n');
            bw.write(Integer.toString(stat.getNumPatientIds()));
            bw.write('\n');
        }
    }

    public TraversalStat loadStat() throws IOException {
        log.debug("Loading stats");

        Path statFilePath = Paths.get(aggregator.getTempDirectoryPath().toString(), Constant.STAT_FILENAME);
        try (
                FileReader fw = new FileReader(statFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw)
        ) {
            int numStudyInstanceUids = Integer.valueOf(bw.readLine());
            int numSeriesInstaneUids = Integer.valueOf(bw.readLine());
            int numSopInstanceUids = Integer.valueOf(bw.readLine());
            int numPatientIds = Integer.valueOf(bw.readLine());

            return new TraversalStat(numStudyInstanceUids, numSeriesInstaneUids, numSopInstanceUids, numPatientIds);
        }
    }
}
