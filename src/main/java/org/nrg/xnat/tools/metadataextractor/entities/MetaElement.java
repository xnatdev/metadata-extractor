package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class MetaElement {
    private String fileName;

    private int tag;

    private int level;

    private String privateCreator;

    private String keyword;

    private String value;

    private List<MetaElement> children = null;
}
