package org.nrg.xnat.tools.metadataextractor.entities.reports;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.entities.AggregateUnit;
import org.nrg.xnat.tools.metadataextractor.entities.AggregatorRecord;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportType;
import org.nrg.xnat.tools.metadataextractor.exceptions.AggregatorRecordParseException;
import org.nrg.xnat.tools.metadataextractor.services.Aggregator;
import org.nrg.xnat.tools.metadataextractor.utils.JsonUtil;
import org.nrg.xnat.tools.metadataextractor.utils.TagUtil;
import org.nrg.xnat.tools.metadataextractor.utils.XmlUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Setter
@Getter
public class ExampleAnalysisAggregate extends AbstractAnalysisAggregate {
    static ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            if (field.getName().equals("aggregator")) {
                return true;
            }
            if (field.getName().equals("totalInputPaths")) {
                return true;
            }
            if (field.getName().equals("numFilesPerValue")) {
                return true;
            }
            if (field.getName().equals("numTruncate")) {
                return true;
            }

            return false;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    @JsonIgnore
    Aggregator aggregator;

    @JsonIgnore
    List<Path> totalInputPaths;

    @JacksonXmlElementWrapper()
    private Map<String, ValueInfo> standardElements = new TreeMap<>();

    @JacksonXmlElementWrapper()
    private Map<String, ValueInfo> privateElements = new TreeMap<>();

    @JsonIgnore
    private int numFilesPerValue;

    public ExampleAnalysisAggregate(int numTruncate, int numFilesPerValue) {
        super(numTruncate);
        this.numFilesPerValue = numFilesPerValue;
    }

    @Override
    public void clear() {
        standardElements.clear();
        privateElements.clear();
    }

    @Override
    public void writeOverallText(Writer writer) throws IOException {
        writeStat(writer);

        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            constructTag(aggregator, totalInputPaths, formattedTag);
            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                formatElementsToText(privateElements, writer);
            } else {
                formatElementsToText(standardElements, writer);
            }
        }
    }

    private void formatElementsToText(Map<String, ValueInfo> elements, Writer writer) throws IOException {
        List<String> standardElementKeys = new ArrayList<>(elements.keySet());
        Collections.sort(standardElementKeys);
        for (String tag : standardElementKeys) {
            ValueInfo valueInfo = elements.get(tag);
            Map<String, List<String>> fileNames = valueInfo.getFileNames();

            writeTextLine(writer);
            writer.write(String.format("%s\t%s\n", tag, valueInfo.getKeyword()));
            writeTextLine(writer);
            List<String> fileNamesKeys = new ArrayList<>(fileNames.keySet());
            Collections.sort(fileNamesKeys);

            int valueCount = 0;

            for (String fileNamesKey : fileNamesKeys) {
                writer.write(fileNamesKey);
                writer.write('\n');

                List<String> names = fileNames.get(fileNamesKey);
                Collections.sort(names);
                int count = 0;

                // When numFilesPerValue == 0, don't display file names
                if (numFilesPerValue != 0) {
                    for (String name : names) {
                        writer.write('\t');
                        writer.write(name);
                        writer.write('\n');

                        count++;
                        if (numFilesPerValue >= 0 && count >= numFilesPerValue) {
                            break;
                        }
                    }
                }

                valueCount++;
                if (numTruncate > 0 && valueCount >= numTruncate) {
                    break;
                }
            }
            writer.write('\n');
        }
    }

    @Override
    public void writeOverallJson(Writer writer) throws IOException {
        writer.write("{");

        // standardElements
        writer.write("\"standardElements\":{");
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        int count = 0;
        writeJson(writer, tagFileNameMap, count, standardElements);
        writer.write("},");

        // privateElements
        writer.write("\"privateElements\":{");
        count = 0;
        writeJson(writer, tagFileNameMap, count, privateElements);
        writer.write("},");

        // stat
        writer.write(String.format("\"stat\":{\"numStudyInstanceUids\":%d,\"numSeriesInstaneUids\":%d,\"numSopInstanceUids\":%d,\"numPatientIds\":%d}", stat.getNumStudyInstanceUids(), stat.getNumSeriesInstaneUids(), stat.getNumSopInstanceUids(), stat.getNumPatientIds()));
        writer.write("}");
    }

    private void writeJson(Writer writer, Map<String, Path> tagFileNameMap, int count, Map<String, ValueInfo> elements) throws IOException {
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                if (elements != privateElements) {
                    continue;
                }
            } else {
                if (elements != standardElements) {
                    continue;
                }
            }

            constructTag(aggregator, totalInputPaths, formattedTag);
            if (elements.size() == 0) {
                continue;
            }
            if (count > 0) {
                writer.write(",");
            }
            JsonUtil.write(elements, writer, numFilesPerValue, numTruncate);

            count++;
        }
    }

    @Override
    public void writeOverallXml(Writer writer) throws IOException {
        writer.write("<" + this.getClass().getSimpleName() + ">\n");

        // stat
        writer.write("  <stat>\n");
        writer.write("    <numStudyInstanceUids>");
        writer.write(Integer.toString(stat.getNumStudyInstanceUids()));
        writer.write("</numStudyInstanceUids>\n");

        writer.write("    <numSeriesInstaneUids>");
        writer.write(Integer.toString(stat.getNumSeriesInstaneUids()));
        writer.write("</numSeriesInstaneUids>\n");

        writer.write("    <numSopInstanceUids>");
        writer.write(Integer.toString(stat.getNumSopInstanceUids()));
        writer.write("</numSopInstanceUids>\n");

        writer.write("    <numPatientIds>");
        writer.write(Integer.toString(stat.getNumPatientIds()));
        writer.write("</numPatientIds>\n");
        writer.write("  </stat>\n");

        // standardElements
        writer.write("  <standardElements>\n");
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();
        int count = 0;
        writeXml(writer, tagFileNameMap, count, standardElements);
        writer.write("  </standardElements>\n");

        // privateElements
        writer.write("  <privateElements>\n");
        count = 0;
        writeXml(writer, tagFileNameMap, count, privateElements);
        writer.write("  </privateElements>\n");

        writer.write("</" + this.getClass().getSimpleName() + ">\n");
    }

    private void writeXml(Writer writer, Map<String, Path> tagFileNameMap, int count, Map<String, ValueInfo> elements) throws IOException {
        for (String formattedTag : tagFileNameMap.keySet()) {
            clear();

            int tag = TagUtil.fromTag(formattedTag);
            if (TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag)) {
                if (elements != privateElements) {
                    continue;
                }
            } else {
                if (elements != standardElements) {
                    continue;
                }
            }

            constructTag(aggregator, totalInputPaths, formattedTag);
            if (elements.size() == 0) {
                continue;
            }
            XmlUtil.write(elements, writer, numFilesPerValue, numTruncate);

            count++;
        }
    }

    @Override
    public void writeStudyText(Writer writer) throws IOException {
        formatElementsToText(standardElements, writer);
        formatElementsToText(privateElements, writer);
    }

    @Override
    public void writeStudyJson(Writer writer) throws IOException {
        try (JsonWriter jsonWriter = new JsonWriter(writer)) {
            Gson gson = new GsonBuilder().addSerializationExclusionStrategy(exclusionStrategy).setPrettyPrinting().create();
            gson.toJson(this, ExampleAnalysisAggregate.class, jsonWriter);
        }
    }

    @Override
    public void writeStudyXml(Writer writer) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.writeValue(writer, this);
    }

    @Override
    public void constructOverall(Aggregator aggregator, List<Path> totalInputPaths) {
        this.aggregator = aggregator;
        this.totalInputPaths = totalInputPaths;
    }

    @Override
    public void constructStudy(Path studyTempFilePath, Aggregator aggregator, List<Path> totalInputPaths) throws IOException {
        clear();

        try (
                FileReader fw = new FileReader(studyTempFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw)
        ) {
            bw.readLine();  // studyInstanceUid

            AggregatorRecord rec;
            Map<String, ValueInfo> map;
            Boolean isPrivate;
            String keyword;
            String formattedTag;
            ValueInfo valueInfo;
            List<String> fileNames;

            for (String line; (line = bw.readLine()) != null; ) {
                try {
                    rec = AggregatorRecord.toObject(line, totalInputPaths);
                } catch (AggregatorRecordParseException e) {
                    log.warn("Invalid record: {} at {}, {}", line, studyTempFilePath, e.getMessage());
                    continue;
                }

                isPrivate = rec.getIsPrivate();
                formattedTag = rec.getFormattedTag();
                keyword = rec.getKeyword();

                map = isPrivate ? privateElements : standardElements;

                if (!map.containsKey(formattedTag)) {
                    valueInfo = new ValueInfo(keyword);
                    map.put(formattedTag, valueInfo);
                } else {
                    valueInfo = map.get(formattedTag);
                }

                if (!valueInfo.getFileNames().containsKey(rec.getValue())) {
                    fileNames = new ArrayList<>();
                    valueInfo.getFileNames().put(rec.getValue(), fileNames);
                } else {
                    fileNames = valueInfo.getFileNames().get(rec.getValue());
                }
                fileNames.add(rec.getFileName());
            }
        }
    }

    public void constructTag(Aggregator aggregator, List<Path> totalInputPaths, String formattedTag) throws IOException {
        Map<String, Path> tagFileNameMap = aggregator.getTagFileNameMap();

        boolean isPrivate;
        String keyword;
        Map<String, ExampleAnalysisAggregate.ValueInfo> map;
        AggregatorRecord rec;
        ExampleAnalysisAggregate.ValueInfo valueInfo;
        List<String> fileNames;

//        for (String formattedTag : tagFileNameMap.keySet()) {
        Path tagFilePath = tagFileNameMap.get(formattedTag);
        try (
                FileReader fw = new FileReader(tagFilePath.toFile());
                BufferedReader bw = new BufferedReader(fw);
        ) {
            bw.readLine();  // Skip formattedTag
            isPrivate = Boolean.valueOf(bw.readLine());
            keyword = bw.readLine();
            map = isPrivate ? privateElements : standardElements;
            for (String line; (line = bw.readLine()) != null; ) {
                try {
                    rec = AggregatorRecord.toObject(line, totalInputPaths);
                } catch (AggregatorRecordParseException e) {
                    log.warn("Invalid record: {} at {}, {}", line, tagFilePath, e.getMessage());
                    continue;
                }

                if (!map.containsKey(formattedTag)) {
                    valueInfo = new ValueInfo(keyword);
                    map.put(formattedTag, valueInfo);
                } else {
                    valueInfo = map.get(formattedTag);
                }

                if (!valueInfo.getFileNames().containsKey(rec.getValue())) {
                    fileNames = new ArrayList<>();
                    valueInfo.getFileNames().put(rec.getValue(), fileNames);
                } else {
                    fileNames = valueInfo.getFileNames().get(rec.getValue());
                }
                fileNames.add(rec.getFileName());
            }
        }
//        }
    }

    @Override
    public String getFilePrefix() {
        return ReportType.example.toString();
    }

    @Getter
    static public class ValueInfo implements AggregateUnit {
        private final String keyword;

        private final Map<String, List<String>> fileNames = new TreeMap<>();

        public ValueInfo(String keyword) {
            this.keyword = keyword;
        }
    }
}
