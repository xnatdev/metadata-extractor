package org.nrg.xnat.tools.metadataextractor.utils;

import org.dcm4che3.data.ElementDictionary;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTag;
import org.nrg.xnat.tools.metadataextractor.entities.CustomTagDictionary;

import java.util.Map;
import java.util.Optional;

public class DictionaryUtil {
    private DictionaryUtil() {
    }

    public static String getKeyword(int tag, String privateCreator, CustomTagDictionary customTagDictionary) {
        if (TagUtils.isPrivateTag(tag)) {
            String formattedTag = String.format("(%04x,\"%s\",%02x)",
                    TagUtils.groupNumber(tag),
                    privateCreator,
                    TagUtils.elementNumber(tag) & 0xff
            );

            Optional<CustomTag> optionalCustomTag = customTagDictionary.search(formattedTag);
            if (!optionalCustomTag.isPresent()) {
                return formattedTag;
            }

            CustomTag customTag = optionalCustomTag.get();
            return customTag.getTagName();
        } else {
            return ElementDictionary.keywordOf(tag, privateCreator);
        }
    }
}
