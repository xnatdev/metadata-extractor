package org.nrg.xnat.tools.metadataextractor.entities.enums;

public enum SupportedScheme {
    http,
    https,
    ftp,
    ftps,
    file
}
