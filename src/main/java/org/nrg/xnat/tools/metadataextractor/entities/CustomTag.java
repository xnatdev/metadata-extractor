package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.Getter;
import org.dcm4che3.data.VR;

@Getter
public class CustomTag {
    private int tag;

    private VR vr;

    private String formattedTag;

    private boolean isPrivate;

    private String privateCreatorId;

    private String tagName;

    public CustomTag(String rawTag, VR vr, String tagName) {
        this.vr = vr;
        this.tagName = tagName;

        if (rawTag == null || rawTag.length() == 0) {
            throw new IllegalArgumentException();
        }
        if (rawTag.chars().filter(ch -> ch == '"').count() % 2 == 1) {
            throw new IllegalArgumentException("count of \" is odd");
        }

        int begPos, endPos;
        if (rawTag.charAt(0) == '(') {
            begPos = 1;
            endPos = rawTag.length() - 2;
        } else {
            begPos = 0;
            endPos = rawTag.length() - 1;
        }

        String[] tokens = rawTag.substring(begPos, endPos + 1).split(",");
        if (tokens.length < 2 || tokens.length > 3) {
            throw new IllegalArgumentException();
        }

        if (tokens.length == 2) {
            this.formattedTag = String.format("(%s,%s)", tokens[0], tokens[1]);
        } else {
            this.privateCreatorId = tokens[1].replaceAll("\"", "");
            this.formattedTag =String.format("(%s,\"%s\",%s)",tokens[0], this.privateCreatorId, tokens[2]);
        }
    }
}
