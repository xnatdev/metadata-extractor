package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.nrg.xnat.tools.metadataextractor.exceptions.AggregatorRecordParseException;
import org.nrg.xnat.tools.metadataextractor.services.Aggregator;
import org.nrg.xnat.tools.metadataextractor.utils.StringUtil;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;

@Setter
@Getter
@ToString
public class AggregatorRecord {
    private String fileName;
    private String value;

    private String formattedTag;
    private String keyword;
    private Boolean isPrivate;

    public static void serialize(BufferedWriter bw, String fileName, String value) throws IOException {
        bw.write('\t');
        bw.write(fileName);
        bw.write('\t');
        bw.write(StringUtil.normalize(value));
        bw.newLine();
    }

    public static void serialize(BufferedWriter bw, String fileName, String value, String formattedTag, Aggregator.ValueInfo valueInfo) throws IOException {
        bw.write('\t');
        bw.write(fileName);
        bw.write('\t');
        bw.write(StringUtil.normalize(value));
        bw.write('\t');
        bw.write(formattedTag);
        bw.write('\t');
        bw.write(valueInfo.getKeyword());
        bw.write('\t');
        bw.write(valueInfo.getIsPrivate().toString());
        bw.newLine();
    }

    public static AggregatorRecord toObject(String raw, List<Path> totalInputPaths) throws AggregatorRecordParseException {
        AggregatorRecord rec = new AggregatorRecord();
        if (raw == null) {
            throw new AggregatorRecordParseException("record is null");
        }
        if (raw.charAt(0) != '\t') {
            throw new AggregatorRecordParseException("record doesn't begin with tab");
        }
        String[] tokens = raw.split("\t");
        if (tokens.length < 2) {
            throw new AggregatorRecordParseException("Invalid aggregator record");
        }
        int filePos;
        try {
            filePos = Integer.parseInt(tokens[1]);
        } catch(NumberFormatException e) {
            throw new AggregatorRecordParseException("Invalid aggregator record: invalid filePos");
        }
        rec.fileName = totalInputPaths.get(filePos).toString();
        rec.value = tokens.length > 2 ? tokens[2] : "";

        // fields for study files
        if (tokens.length == 6) {
            rec.formattedTag = tokens[3];
            rec.keyword = tokens[4];
            rec.isPrivate = Boolean.valueOf(tokens[5]);
        }
        return rec;
    }
}
