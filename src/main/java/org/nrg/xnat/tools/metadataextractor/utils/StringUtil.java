package org.nrg.xnat.tools.metadataextractor.utils;

public class StringUtil {
    private StringUtil() {
    }

    public static String normalize(String raw) {
        return raw.replaceAll("\r", "")
                .replaceAll("\n", " ")
                .replaceAll("\t", " ");
    }
}
