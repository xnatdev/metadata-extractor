package org.nrg.xnat.tools.metadataextractor.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TraversalStat {

    private int numStudyInstanceUids;

    private int numSeriesInstaneUids;

    private int numSopInstanceUids;

    private int numPatientIds;
}
