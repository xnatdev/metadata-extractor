package org.nrg.xnat.tools.metadataextractor.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dcm4che3.util.TagUtils;
import org.nrg.xnat.tools.metadataextractor.common.Constant;
import org.nrg.xnat.tools.metadataextractor.common.InputArgument;
import org.nrg.xnat.tools.metadataextractor.entities.*;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportGranularity;
import org.nrg.xnat.tools.metadataextractor.utils.FileUtil;
import org.nrg.xnat.tools.metadataextractor.utils.StringUtil;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;

@Slf4j
@Getter
public class Aggregator {
    private static final Object _syncTemp = new Object();

    private final Map<String, Path> tagFileNameMap = new TreeMap<>();
    private final Map<String, Path> studyFileNameMap = new TreeMap<>();
    private final Map<String, ValueInfo> tempOverallMap = new TreeMap<>();
    private final Map<String, Map<String, ValueInfo>> tempStudiesMap = new TreeMap<>();

    private final Path tempDirectoryPath;
    private final Boolean keepFiles;
    private final Boolean isWriteOnly;
    private final InputArgument inputArgument;

    private int lastCommitCount = 0;
    private int count = 0;

    public Aggregator(InputArgument inputArgument) throws IOException {
        this.inputArgument = inputArgument;
        this.keepFiles = inputArgument.getKeepFiles();
        this.isWriteOnly = inputArgument.getWriteOnly();

        File tempDirectoryFile = inputArgument.getIntermediateDirectory();
        if (tempDirectoryFile == null) {
            this.tempDirectoryPath = FileUtil.createTempDirectory();
        } else {
            this.tempDirectoryPath = tempDirectoryFile.toPath();
            if (isWriteOnly) {
                load();
            } else {
                FileUtils.cleanDirectory(tempDirectoryFile);
            }
        }
    }

    public void load() throws IOException {
        List<Path> inputPaths = new ArrayList<>();
        Files.walkFileTree(tempDirectoryPath, EnumSet.of(FOLLOW_LINKS), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
                if (tempDirectoryPath.relativize(path).toString().startsWith("(") && path.toString().endsWith(".tmp")) {
                    log.debug("tmp file found: {}", path);
                    inputPaths.add(path);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                // This is important to note. Test this behaviour
                return FileVisitResult.CONTINUE;
            }
        });

        for (Path path : inputPaths) {
            try (
                    FileReader fileReader = new FileReader(path.toString());
                    BufferedReader reader = new BufferedReader(fileReader)
            ) {
                String formattedTag = reader.readLine();
                tagFileNameMap.put(formattedTag, path);
            }
        }
    }

    public void postProcess() {
        if (keepFiles) {
            log.debug("Keeping intermediate files in {}", tempDirectoryPath);
        } else {
            try {
                System.out.printf("Removing intermediate files in %s\n", tempDirectoryPath);
                log.debug("Removing intermediate files in {}", tempDirectoryPath);
                FileUtil.deleteTempDirectory(tempDirectoryPath);
            } catch (IOException e) {
                log.warn("Failed to temporary directory{}, {}", tempDirectoryPath, e.toString());
            }
        }
    }

    public void aggregate(Metadata metadata) throws IOException {
        aggregate(metadata.getElements(), new ArrayList<>(), metadata);

        synchronized (_syncTemp) {
            count++;
            if (count % Constant.CHUNK_COUNT == 0) {
                appendPermanently();
            }
        }
    }

    private void aggregate(List<MetaElement> elements, List<String> parentTags, Metadata metadata) {
        for (MetaElement element : elements) {
            if (element.getChildren() != null) {
                parentTags.add(TagUtils.toString(element.getTag()));
                aggregate(element.getChildren(), parentTags, metadata);
                parentTags.remove(parentTags.size() - 1);
            } else {
                keepAggregateInMemory(element, parentTags, metadata);
            }
        }
    }

    private void keepAggregateInMemory(MetaElement element, List<String> parentTags, Metadata metadata) {
        String value = element.getValue();
        String formattedTag =
                (parentTags != null ? String.join("", parentTags) : "")
                        + TagUtils.toString(element.getTag());
        Boolean isPrivateTag = isPrivateTag(element.getTag());
        String studyInstanceUid = metadata.getStudyInstanceUid();

        String strPos = Integer.toString(metadata.getPos());

        synchronized (_syncTemp) {
            // Overall aggregation
            appendToMap(element, value, formattedTag, isPrivateTag, strPos, tempOverallMap);

            // Aggregation per study
            if (inputArgument.getStudyReportProduced()) {

                Map<String, ValueInfo> studyMap;
                if (!tempStudiesMap.containsKey(studyInstanceUid)) {
                    studyMap = new TreeMap<>();
                    tempStudiesMap.put(studyInstanceUid, studyMap);
                } else {
                    studyMap = tempStudiesMap.get(studyInstanceUid);
                }

                appendToMap(element, value, formattedTag, isPrivateTag, strPos, studyMap);
            }
        }
    }

    private void appendToMap(MetaElement element, String value, String formattedTag, Boolean isPrivateTag, String strPos, Map<String, ValueInfo> tempMap) {
        ValueInfo valueInfo;
        List<String> fileNames;
        if (!tempMap.containsKey(formattedTag)) {
            valueInfo = new ValueInfo(isPrivateTag, element.getKeyword());
            tempMap.put(formattedTag, valueInfo);
        } else {
            valueInfo = tempMap.get(formattedTag);
        }

        if (!valueInfo.getFileNames().containsKey(value)) {
            fileNames = new ArrayList<>();
            valueInfo.getFileNames().put(value, fileNames);
        } else {
            fileNames = valueInfo.getFileNames().get(value);
        }
        fileNames.add(strPos);
    }

    public void appendPermanently() throws IOException {
        if (lastCommitCount == count) {
            return;
        }
        appendOverallPermanently();
        if (inputArgument.getStudyReportProduced()) {
            appendStudiesPermanently();
        }
    }

    public void appendOverallPermanently() throws IOException {
        for (String formattedTag : tempOverallMap.keySet()) {
            ValueInfo valueInfo = tempOverallMap.get(formattedTag);

            if (!tagFileNameMap.containsKey(formattedTag)) {
                Path path = createTagFile(formattedTag, valueInfo.getIsPrivate(), valueInfo.getKeyword());
                tagFileNameMap.put(formattedTag, path);
            }
            Path path = tagFileNameMap.get(formattedTag);

            try (
                    FileWriter fw = new FileWriter(path.toString(), true);
                    BufferedWriter bw = new BufferedWriter(fw)
            ) {
                for (String value : valueInfo.getFileNames().keySet()) {
                    List<String> fileNames = valueInfo.getFileNames().get(value);
                    for (String fileName : fileNames) {
                        AggregatorRecord.serialize(bw, fileName, value);
                    }
                }
            }
        }

        tempOverallMap.clear();
        lastCommitCount = count;
    }

    public void appendStudiesPermanently() throws IOException {
        for (String studyInstanceUid : tempStudiesMap.keySet()) {
            Map<String, ValueInfo> studyMap = tempStudiesMap.get(studyInstanceUid);

            if (!studyFileNameMap.containsKey(studyInstanceUid)) {
                Path path = createStudyFile(studyInstanceUid);
                studyFileNameMap.put(studyInstanceUid, path);
            }
            Path path = studyFileNameMap.get(studyInstanceUid);

            try (
                    FileWriter fw = new FileWriter(path.toString(), true);
                    BufferedWriter bw = new BufferedWriter(fw)
            ) {
                for (String formattedTag : studyMap.keySet()) {
                    ValueInfo valueInfo = studyMap.get(formattedTag);

                    for (String value : valueInfo.getFileNames().keySet()) {
                        List<String> fileNames = valueInfo.getFileNames().get(value);
                        for (String fileName : fileNames) {
                            AggregatorRecord.serialize(bw, fileName, value, formattedTag, valueInfo);
                        }
                    }
                }
            }
        }

        tempStudiesMap.clear();
    }

    private boolean isPrivateTag(int tag) {
        return TagUtils.isPrivateTag(tag) || TagUtils.isPrivateGroup(tag);
    }

    private Path createTagFile(String formattedTag, boolean isPrivate, String keyword) throws IOException {
        Path path = FileUtil.createTempFile(tempDirectoryPath, formattedTag);
        synchronized (Constant.SYNC_FILE_WRITE) {
            try (
                    FileWriter fw = new FileWriter(path.toString(), true);
                    BufferedWriter bw = new BufferedWriter(fw, Constant.BUFFER_SIZE)
            ) {
                bw.write(formattedTag);
                bw.newLine();
                bw.write(Boolean.toString(isPrivate));
                bw.newLine();
                bw.write(keyword);
                bw.newLine();
            }
        }
        return path;
    }

    private Path createStudyFile(String studyInstanceUid) throws IOException {
        Path path = FileUtil.createTempFile(tempDirectoryPath, studyInstanceUid);
        synchronized (Constant.SYNC_FILE_WRITE) {
            try (
                    FileWriter fw = new FileWriter(path.toString(), true);
                    BufferedWriter bw = new BufferedWriter(fw, Constant.BUFFER_SIZE)
            ) {
                bw.write(studyInstanceUid);
                bw.newLine();
            }
        }
        return path;
    }

    @Getter
    static public class ValueInfo implements AggregateUnit {
        private final Boolean isPrivate;
        private final String keyword;
        private final Map<String, List<String>> fileNames = new TreeMap<>();

        public ValueInfo(Boolean isPrivate, String keyword) {
            this.isPrivate = isPrivate;
            this.keyword = keyword;
        }
    }
}
