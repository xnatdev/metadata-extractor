package org.nrg.xnat.tools.metadataextractor.entities.reports;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.nrg.xnat.tools.metadataextractor.entities.TraversalStat;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportFormat;
import org.nrg.xnat.tools.metadataextractor.services.Aggregator;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.List;

@Getter
@Setter
public abstract class AbstractAnalysisAggregate {
    @JsonIgnore
    protected int numTruncate;

    @JsonIgnore
    protected TraversalStat stat;

    public AbstractAnalysisAggregate(int numTruncate) {
        this.numTruncate = numTruncate;
    }

    public void writeOverall(ReportFormat reportFormat, Writer writer) throws IOException {
        switch (reportFormat) {
            case text:
                writeOverallText(writer);
                break;

            case json:
                writeOverallJson(writer);
                break;

            case xml:
                writeOverallXml(writer);
                break;

            default:
                throw new IllegalArgumentException("Invalid report format");
        }
    }

    public void writeStudy(ReportFormat reportFormat, Writer writer) throws IOException {
        switch (reportFormat) {
            case text:
                writeStudyText(writer);
                break;

            case json:
                writeStudyJson(writer);
                break;

            case xml:
                writeStudyXml(writer);
                break;

            default:
                throw new IllegalArgumentException("Invalid report format");
        }
    }

    abstract public void writeOverallText(Writer writer) throws IOException;

    abstract public void writeOverallJson(Writer writer) throws IOException;

    abstract public void writeOverallXml(Writer writer) throws IOException;

    abstract public void writeStudyText(Writer writer) throws IOException;

    abstract public void writeStudyJson(Writer writer) throws IOException;

    abstract public void writeStudyXml(Writer writer) throws IOException;

    public abstract void constructOverall(Aggregator aggregator, List<Path> totalInputPaths) throws IOException;

    public abstract void constructStudy(Path studyTempFilePath, Aggregator aggregator, List<Path> totalInputPaths) throws IOException;

    @JsonIgnore
    abstract public String getFilePrefix();

    abstract public void clear();

    public void writeTextLine(Writer writer) throws IOException {
        writer.write("==============================================\n");
    }

    public void writeTextHeader(Writer writer, String title) throws IOException {
        writeTextLine(writer);
        writer.write(title);
        writer.write("\n");
        writeTextLine(writer);
    }

    public void writeStat(Writer writer) throws IOException {
        writeTextHeader(writer, "Overall");
        writer.write(String.format("Number of Patient IDs encountered: %d\n", stat.getNumPatientIds()));
        writer.write(String.format("Number of Study Instance UIDs encountered: %d\n", stat.getNumStudyInstanceUids()));
        writer.write(String.format("Number of Series Instance UIDs encountered: %d\n", stat.getNumSeriesInstaneUids()));
        writer.write(String.format("Number of SOP Instance UIDs encountered: %d\n", stat.getNumSopInstanceUids()));
        writer.write("\n");
    }
}
