package org.nrg.xnat.tools.metadataextractor.common;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportFormat;
import org.nrg.xnat.tools.metadataextractor.entities.enums.ReportType;
import org.nrg.xnat.tools.metadataextractor.entities.enums.SupportedScheme;
import org.nrg.xnat.tools.metadataextractor.entities.enums.UniqueType;
import org.nrg.xnat.tools.metadataextractor.utils.FileUtil;
import org.nrg.xnat.tools.metadataextractor.utils.FtpUtil;
import org.nrg.xnat.tools.metadataextractor.utils.HttpUtil;
import picocli.CommandLine;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static org.nrg.xnat.tools.metadataextractor.common.Constant.DEFAULT_DICTIONARY_FILENAME;

@CommandLine.Command(
        name = Constant.APP_NAME,
        mixinStandardHelpOptions = true,
        version = Constant.APP_VERSION,
        description = "Produces DICOM Metadata analysis reports",
        usageHelpWidth = 120
)
@ToString
@Slf4j
public class InputArgument implements Callable<Integer> {
    private static final String SPLIT = ",";

    @CommandLine.Option(
            names = {"-i", "--source-directories"},
            defaultValue = Constant.DEFAULT_INPUT_DIRECTORY,
            split = InputArgument.SPLIT,
            description = "Comma-separated input directories (Default: ${DEFAULT-VALUE})"
    )
    private List<File> inputDirectories;

    @CommandLine.Option(
            names = {"-o", "--output-directory"},
            defaultValue = Constant.DEFAULT_OUTPUT_DIRECTORY,
            description = "Output directory that stores the created reports (Default: ${DEFAULT-VALUE})"
    )
    private File outputDirectory;

    @CommandLine.Option(
            names = {"-r", "--report"},
            defaultValue = Constant.DEFAULT_REPORT_TYPES,
            split = InputArgument.SPLIT,
            description = "Comma-separated report types to produce: summary (Default: ${DEFAULT-VALUE})"
    )
    private List<ReportType> reportTypes;

    @CommandLine.Option(
            names = {"-f", "--format"},
            defaultValue = Constant.DEFAULT_REPORT_FORMATS,
            split = InputArgument.SPLIT,
            description = "Comma-separated report formats to produce: json,xml,text (Default: ${DEFAULT-VALUE})"
    )
    private List<ReportFormat> reportFormats;

    @CommandLine.Option(
            names = {"-s", "--study-report"},
            defaultValue = "False",
            description = "Whether to produce per-study reports"
    )
    private Boolean studyReportProduced;

    @CommandLine.Option(
            names = {"-d", "--dictionaries"},
            split = InputArgument.SPLIT,
            description = "Optional comma-separated dictionary file path (http, ftp or file)"
    )
    private List<String> dictionaryUrls = new ArrayList<>();

    private List<File> dictionaryFiles = new ArrayList<>();

    @CommandLine.Option(
            names = {"-t", "--truncate"},
            defaultValue = Constant.DEFAULT_NUMBER_OF_TRUNCATE,
            description = "number of unique element to be shown (Default: ${DEFAULT-VALUE})"
    )
    private int numTruncate;

    @CommandLine.Option(
            names = {"-nf", "--number-of-files"},
            defaultValue = Constant.DEFAULT_NUMBER_OF_FILES_PER_VALUE,
            description = "number of files per value to be shown in the \"example\" type report file (Default: ${DEFAULT-VALUE}). Set -1 to list all files"
    )
    private int numFilesPerValue;

    @CommandLine.Option(
            names = {"-T", "--thread"},
            defaultValue = Constant.DEFAULT_NUMBER_OF_THREADS,
            description = "number of threads to run (Default: ${DEFAULT-VALUE})"
    )
    private int numThreads;

    @CommandLine.Option(
            names = {"-u", "--unique"},
            defaultValue = Constant.DEFAULT_UNIQUE_TYPE,
            description = "processes a single DICOM per study or series: none, study, or series (Default: ${DEFAULT-VALUE})"
    )
    private UniqueType uniqueType;

    @CommandLine.Option(
            names = {"-k", "--keep"},
            defaultValue = "False",
            description = "whether to keep intermediate files"
    )
    private Boolean keepFiles;

    @CommandLine.Option(
            names = {"-I", "--intermediate directory"},
            description = "Intermediate Directory"
    )
    private File intermediateDirectory;

    @CommandLine.Option(
            names = {"-O", "--write-only"},
            defaultValue = "False",
            description = "Write Only"
    )
    private Boolean isWriteOnly;

    public List<ReportType> getReportTypes() {
        return reportTypes;
    }

    @CommandLine.Option(
            names = {"-l", "--dicom-file-list"},
            description = "Dicom File List"
    )
    private File dicomListFile;

    public List<ReportFormat> getReportFormats() {
        return reportFormats;
    }

    public List<File> getInputDirectories() {
        return inputDirectories;
    }

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public List<File> getDictionaryFiles() {
        return dictionaryFiles;
    }

    public UniqueType getUniqueType() {
        return uniqueType;
    }

    public int getNumTruncate() {
        return numTruncate;
    }

    public int getNumFilesPerValue() {
        return numFilesPerValue;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public Boolean getKeepFiles() {
        return keepFiles;
    }

    public File getIntermediateDirectory() {
        return intermediateDirectory;
    }

    public Boolean getWriteOnly() {
        return isWriteOnly;
    }

    public File getDicomListFile() {
        return dicomListFile;
    }

    public Boolean getStudyReportProduced() {
        return studyReportProduced;
    }

    @Override
    public Integer call() throws Exception { // your business logic goes here...
        // Validate input arguments
        for (File dictionaryFile : dictionaryFiles) {
            if (!dictionaryFile.exists()) {
                throw new IllegalArgumentException(String.format("%s doesn't exist", dictionaryFile.getAbsolutePath()));
            }
        }
        String workingDirectory = System.getProperty("user.dir");

        // Handle dictionary files
        for (String rawUrl : dictionaryUrls) {
            if (rawUrl == null || rawUrl.length() == 0) {
                continue;
            }
            String adjustedUrl = rawUrl;
            if (adjustedUrl.indexOf(":") == -1) {
                if (adjustedUrl.charAt(0) == '/') {
                    adjustedUrl = "file://" + adjustedUrl;
                } else {
                    adjustedUrl = "file://" + workingDirectory + File.separator + adjustedUrl;
                }
            }

            URL url = new URL(adjustedUrl);
            SupportedScheme scheme = SupportedScheme.valueOf(url.getProtocol());
            File dictionaryFile = null;
            switch (scheme) {
                case ftp:
                case ftps:
                    dictionaryFile = FtpUtil.download(url);
                    break;

                case http:
                case https:
                    dictionaryFile = HttpUtil.download(url);
                    break;

                case file:
                    dictionaryFile = new File(url.getFile()).getCanonicalFile();
                    if (dictionaryFile.isDirectory()) {
                        throw new IllegalArgumentException("directory is not allowed: " + dictionaryFile);
                    }
                    break;
            }

            if (dictionaryFile != null) {
                dictionaryFiles.add(dictionaryFile);
            }
        }

        if (dictionaryFiles.size() == 0) {
            System.out.println("Default dictionary will be used");
            log.debug("Default dictionary will be used");
            File file = FileUtil.loadResourceFile(DEFAULT_DICTIONARY_FILENAME);
            dictionaryFiles.add(file);
            log.debug("Dictionary file is saved to {}", file);
        }

        int cores = Runtime.getRuntime().availableProcessors();
        numThreads = Math.max(1, Math.min(cores, numThreads));
        log.debug("# of thread to use is {}", numThreads);

        // Validate dicom file list
        if (dicomListFile != null) {
            if (!dicomListFile.exists()) {
                throw new IllegalArgumentException(String.format("The Dicom file list (%s) doesn't exist", dicomListFile.getAbsolutePath()));
            }
            if (!dicomListFile.isFile()) {
                throw new IllegalArgumentException(String.format("The Dicom file list (%s) shoud be a file", dicomListFile.getAbsolutePath()));
            }
        }

        // Validate write-only option
        if (isWriteOnly == true) {
            if (intermediateDirectory == null) {
                throw new IllegalArgumentException(String.format("intermediate directory should be set."));
            }
        }

        return 0;
    }
}
