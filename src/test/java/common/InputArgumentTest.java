package common;

import org.junit.jupiter.api.Test;
import org.nrg.xnat.tools.metadataextractor.common.Constant;
import org.nrg.xnat.tools.metadataextractor.common.InputArgument;
import picocli.CommandLine;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class InputArgumentTest {
    @Test
    public void CommandLineProducesInputArgument() {
        InputArgument inputArgument = new InputArgument();
        CommandLine commandLine = new CommandLine(inputArgument);
        int exitCode = commandLine.execute();

        assertEquals(0, exitCode);
        assertEquals(Constant.DEFAULT_INPUT_DIRECTORY, inputArgument.getInputDirectories().get(0).toString());
        assertEquals(Constant.DEFAULT_OUTPUT_DIRECTORY, inputArgument.getOutputDirectory().toString());
    }
}
