package org.nrg.xnat.tools.metadataextractor.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TagUtilTest {
    @Test
    void getLastTagOfMultiTag() {
        String before = "(0054,0220)(0054,0222)";
        String after = TagUtil.getLastTag(before);
        assertEquals("(0054,0222)", after);
    }

    @Test
    void getLastTagOfSingleTag() {
        String before = "(0054,0222)";
        String after = TagUtil.getLastTag(before);
        assertEquals("(0054,0222)", after);
    }

    @Test
    void getUnformattedTag() {
        String before = "(0054,0222)";
        String after = TagUtil.removeFormatFromTag(before);
        assertEquals("00540222", after);
    }

    @Test
    void checkFromTag() {
        String before = "(0020,000D)";
        int tag = TagUtil.fromTag(before);
        assertEquals(2097165, tag);
    }
}
