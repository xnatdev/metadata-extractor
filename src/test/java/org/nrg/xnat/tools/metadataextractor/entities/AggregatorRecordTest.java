package org.nrg.xnat.tools.metadataextractor.entities;

import org.junit.jupiter.api.Test;
import org.nrg.xnat.tools.metadataextractor.utils.FileUtil;

import java.io.*;
import java.nio.file.Path;

public class AggregatorRecordTest {
    @Test
    public void checkSerialization() throws IOException {
        Path path = FileUtil.createTempFile();
        try (
                FileWriter fw = new FileWriter(path.toFile());
                BufferedWriter bw = new BufferedWriter(fw)
        ) {
            AggregatorRecord.serialize(bw, "fileName", "value");
        }

        try (
                FileReader fr = new FileReader(path.toFile());
                BufferedReader br = new BufferedReader(fr)
        ) {
            System.out.println(br.readLine());
        }
    }
}
