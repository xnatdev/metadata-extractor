package org.nrg.xnat.tools.metadataextractor.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonUtilTest {
    @Test
    void escapeQuote() {
        String before = "(0029,\"CARDIO-D.R. 1.0\",02)";
        String after = JsonUtil.escape(before);
        assertEquals("(0029,\\\"CARDIO-D.R. 1.0\\\",02)", after);
    }

}
