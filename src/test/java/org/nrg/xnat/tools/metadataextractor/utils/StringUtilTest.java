package org.nrg.xnat.tools.metadataextractor.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringUtilTest {
    @Test
    void normalizeRemovesSpecialCharacters() {
        String before = "a\nb\tc\r";
        String after = StringUtil.normalize(before);
        assertEquals("a b c", after);
    }

    @Test
    void normalizeRemovesNewLines() {
        String before = "a\r\nb";
        String after = StringUtil.normalize(before);
        assertEquals("a b", after);
    }
}
