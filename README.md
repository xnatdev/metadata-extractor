# Metadata Extractor

This application scans through one or more inputdirectoies, finds DICOM files, and extract metadata from them.

## Download

Download the latest JAR file from https://bitbucket.org/xnatdev/metadata-extractor/downloads/

## Usage

```code
java -jar metadata-extractor-0.0.2-SNAPSHOT-fat.jar -h

Usage: metadata-extractor [-hV] [-o=<outputDirectory>] [-t=<numTruncate>] [-d=<dictionaryFiles>[,
<dictionaryFiles>...]]... [-f=<reportFormats>[,<reportFormats>...]]... [-i=<inputDirectories>
[,<inputDirectories>...]]... [-r=<reportTypes>[,<reportTypes>...]]...
Produces DICOM Metadata analysis reports
-d, --dictionaries=<dictionaryFiles>[,<dictionaryFiles>...]
Optional comma-separated dictionary files
-f, --format=<reportFormats>[,<reportFormats>...]
Comma-separated report formats to produce: json,xml,text (Default: json,text,xml)
-h, --help Show this help message and exit.
-i, --source-directories=<inputDirectories>[,<inputDirectories>...]
Comma-separated input directories (Default: /input)
-o, --output-directory=<outputDirectory>
Output directory that stores the created reports (Default: /output)
-r, --report=<reportTypes>[,<reportTypes>...]
Comma-separated report types to produce: summary (Default: summary,example,present)
-t, --truncate=<numTruncate>
number of unique element to be shown (Default: 0)
-V, --version Print version information and exit.
```

## Build

```code
./gradlew fatJar
```

## Report Types

### Present

#### Present - Text

```text
(0002,0000) [FileMetaInformationGroupLength] [Frequency: 373]
(0002,0001) [FileMetaInformationVersion] [Frequency: 373]
(0002,0002) [MediaStorageSOPClassUID] [Frequency: 373]
...
```

#### Presnt - JSON

```json
{
  "standardElements": {
    "(0002,0000)": {
      "keyword": "FileMetaInformationGroupLength",
      "frequency": 373
    },
    ...
  }
    "privateElements": {
    "(0009,0010)": {
      "keyword": "PrivateCreatorID",
      "frequency": 4
    },
    ...
  }
}
```

#### Presnt - XML

```xml
<PresentAnalysisAggregate>
  <numTruncate>0</numTruncate>
  <standardElements>
    <(0002,0000)>
      <keyword>FileMetaInformationGroupLength</keyword>
      <frequency>373</frequency>
    </(0002,0000)>
```

### Summary

#### Summary - Text

```text
==============================================
(0002,0000)	FileMetaInformationGroupLength
==============================================
158 [Frequency: 2]
176 [Frequency: 1]
192 [Frequency: 2]
194 [Frequency: 3]
196 [Frequency: 1]
214 [Frequency: 2]
230 [Frequency: 361]
```

#### Summary - JSON

```json
{
  "standardElements": {
    "(0002,0000)": {
      "keyword": "FileMetaInformationGroupLength",
      "counters": {
        "158": 2,
        "176": 1,
        "192": 2,
        "194": 3,
        "196": 1,
        "214": 2,
        "230": 361
      }
    },
    ...
  },
  "privateElements": {
    "(0009,0010)": {
      "keyword": "PrivateCreatorID",
      "counters": {
        "FDMS 1.0": 1,
        "GEMS_IDEN_01": 1,
        "SPI RELEASE 1": 1
      }
    },
    ...
  }
}
```

#### Summary - XML

```xml
<SummaryAnalysisAggregate>
  <numTruncate>0</numTruncate>
  <standardElements>
    <(0002,0000)>
      <keyword>FileMetaInformationGroupLength</keyword>
      <counters>
        <158>2</158>
        <176>1</176>
        <192>2</192>
        <194>3</194>
        <196>1</196>
        <214>2</214>
        <230>361</230>
      </counters>
    </(0002,0000)>
```

### Example

#### Example - Text

#### Example - JSON

```json
{
  "standardElements": {
    "(0002,0000)": {
      "keyword": "FileMetaInformationGroupLength",
      "fileNames": {
        "158": [
          "/Users/woonchan/Documents/sample/dcmjs-sample/bmode.dcm",
          "/Users/woonchan/Documents/sample/dcmjs-sample/ttfm.dcm"
        ],
        ...
      }
    }
  },
  "privateElements": {
    "(0009,0010)": {
      "keyword": "PrivateCreatorID",
      "fileNames": {
        "FDMS 1.0": [
          "/Users/woonchan/Documents/sample/dcmjs-sample/sample1.dcm"
        ],
        "GEMS_IDEN_01": [
          "/Users/woonchan/Documents/sample/compsamples_jpegls/IMAGES/JLSL/CT1_JLSL.dcm"
        ],
        "SPI RELEASE 1": [
          "/Users/woonchan/Documents/sample/dcmjs-sample/sample2.dcm"
        ]
      }
    },
}
```

#### Example - XML

```xml
<ExampleAnalysisAggregate>
  <numTruncate>0</numTruncate>
  <standardElements>
    <(0002,0000)>
      <keyword>FileMetaInformationGroupLength</keyword>
      <fileNames>
        <158>/Users/woonchan/Documents/sample/dcmjs-sample/bmode.dcm</158>
        <158>/Users/woonchan/Documents/sample/dcmjs-sample/ttfm.dcm</158>
```

## Dictionary File format

```code
element_sig_pattern,tag_name,vr,element_seen_id,private_disposition
"(0009,"GEIIS",05)","UN","Unknown","1457","d"
"(0008,1084)[<0>](312f,"Ramsoft Diagnosis Datetime Identifier",10)","DA","Admitting Diagnoses Code Sequence:Unknown","203982",""
```
